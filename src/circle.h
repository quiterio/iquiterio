#ifndef  CRL__H
#define CRL__H

#include "rect.h"
#include "list.h"

typedef void* Circle;
/*Modela a forma geométrica circulo. Um circulo é representado pelas coordenadas de seu centro,
raio,cor de preenchimento e de borda, sendo essas cores as validas definidas pelo padrão SVG*/
Circle crtCrcl(float pX, float pY, float pR, char clrFill[20], char clrStroke[20], int pId, char pType);
/*A variável pR(raio) deve conter um valor maior que zero, As variáveis clrFill eclrStroke devem receber
uma String que contenha o nome ou código hexadecimal de uma cor validadefinida pelo padrão SVG*/
/*Retorna um circulo(circle) com centro pX e pY, raio pR, cor de preenchimentoclrFill e de borda clrStroke*/
void chngInptClrCirc(char *newFll, char *newStrk, char *fill, char *stroke);
void cmpCirc(float x, float y, float r, char *c1, char *c2, List *shp);
void slctShpCmpC(float x, float y,float r, char *c1, char *c2, List *shp, int i, int *n);
void chngClrC(Circle circl, char *newcolor);

float getXc(Circle circl);
//Retorna o valor de X do circulo(Circle)
float getYc(Circle circl);
//Retorna o valor de Y do circulo(Circle)
float getRc(Circle circl);
//Retorna o valor de Raio do circulo(Circle)
char* getFllC(Circle circl);
//Retorna o nome ou código da cor de preenchimento usado no circulo(Circle)
char* getStrkC(Circle circl);
//Retorna o nome ou código da cor de borda usado no circulo(Circle)
void setCrclPnt(Circle circl, char p[10]);
char* getCrclPnt(Circle circl);
int cmpCrclTree(Circle circl1, Circle circl2);

#endif
