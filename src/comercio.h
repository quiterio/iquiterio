#ifndef COM_H_
#define COM_H_
#include <stdio.h>
#include <stdlib.h>
#include "city.h"

typedef void* Comercio;

void arqTpEst(FILE *inpt, City city);
void arqEstCom(FILE *arq, City city);
Comercio criarEstblcmnt(char *cnpj, char *cod, char *nome, char *cep, char face, int num);
Comercio criarTpCmrc(char *tp, char *des);
int compTreeCodTp(Comercio comercio1, Comercio comercio2);
int compTreeComercio(Comercio comercio1, Comercio comercio2);

char* getEnderecoComercio(Comercio comercio);
char* getNomeComercio(Comercio comercio);
char* getCepEstCom(Comercio comercio);
char* getCodTpEstCom(Comercio comercio);
char* getDescricao(Comercio comercio);
int getNmrEstCom(Comercio comercio);
char getFaceEstCom(Comercio comercio);
char* getCNPJEstCom(Comercio comercio);
void vrfcBlcksCodTp(FILE *outTxt, char *cod, City city);
void escreveNomeEndDes(FILE *arq, List listaEstCom, City city);
void dstrsComercio(Comercio comercio);
void dstrsTipoComercio(Comercio comercio);

// void printComercio(Comercio comercio1);

#endif
