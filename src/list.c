#include <stdlib.h>
#include <stdio.h>
#include "list.h"

struct lis{
  Item item;
  struct lis* next;
  struct lis* previous;
};
struct nod{
  struct lis* first;
  struct lis* last;
  int len;
};

List createList(){
  struct nod* node = (struct nod*)malloc(sizeof(struct nod));

  node->first = NULL;
  node->last = NULL;
  node->len = 0;

  return node;
}

void pushFront(List *i, Item obj){
  struct nod* node = (struct nod*)*i;
  struct lis* list = node->first;
  struct lis* new = (struct lis*)malloc(sizeof(struct lis));
  new->item = obj;
  if (itsEmpty(*i)){
    new->previous = NULL;
    new->next = NULL;
    node->last = new;
  }
  else{
    list->previous = new;
    new->previous = NULL;
    new->next = list;
  }
  node->first = new;
  node->len += 1;
}

void pushBack(List *i, Item obj){
  struct nod* node = (struct nod*)*i;
  struct lis* list = node->last;
  if (itsEmpty(*i)) pushFront(i, obj);
  else{
    struct lis* new = (struct lis*)malloc(sizeof(struct lis));
    new->item = obj;
    new->previous = list;
    new->next = NULL;
    list->next = new;
    node->last = new;
    node->len +=1;
  }
}

Item getItemN(List i, int n)
{
  int j;
  struct nod* node = (struct nod*)i;
  struct lis* list = node->first;
  for (j = 0; j < n; j++){
    list = list->next;
  }
  return list->item;
}

int getLen(List *i){
  struct nod* node = (struct nod*)i;
  return node->len;
}

void popFront(List i){
  struct nod* node = (struct nod*)i;
  struct lis* list = node->first;
  list = list->next;
  node->first = list;
  free(list->previous);
  list->previous = NULL;
  node->len -= 1;
}

void popBack(List i){
  struct nod* node = (struct nod*)i;
  struct lis* list = node->last;
  list = list->previous;
  free(list->next);
  list->next = NULL;
  node->len -= 1;
}

void freeAll(List *i){
  struct nod* node = (struct nod*)i;
  struct lis* list = node->last;

  for (list; list != NULL; list = list->previous){
    if(list->next != NULL) popBack(i);
  }
  free(node->first);
  node->first = NULL;
  node->last = NULL;
  node->len = 0;
}

int itsEmpty(List i){
  struct nod* node = (struct nod*)i;
  if(node->len == 0) return 1;
  return 0;
}

void popItemN(List *lista, int n){
  struct nod* node = (struct nod*)lista;
  struct lis* list = node->first;
  for(int j=0;j<n;j++){
    list = list->next;
  }
  struct lis* prev = list->previous;
  prev->next = list->next;
  free(list);
}
