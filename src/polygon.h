#ifndef POL__H
#define POL__H
#include <stdio.h>
#include "list.h"

typedef void* Vertex;
/*Modela um polígono(Polygon) representado por uma lista(List) de vértices que são ligados
sequencialmente. Vértice é um elemento de Polygon formado pelas coordenadas x e y*/
List creatPol(FILE *inptPol);
//inptPol deve ser um arquivo de texto que contenha coordenadas dos pontos relativos aos vértices
//Retorna uma lista(List) contendo os pontos relativos aos vértices

void addVrtx(List *lPlgn, float x, float y);
int pointInPolygon(List *lPlgn, float x, float y);
int onSegment(float x1, float y1, float x2, float y2, float x3, float y3);
int doIntersect(float x1, float y1, float x2, float y2, float xP1, float yP1, float xP2, float yP2);
int orientation(float x1, float y1, float x2, float y2, float x3, float y3);
int runPolygon(List *lPlgn, float x1, float y1, float x2, float y2);

float getXPlgn(Vertex plgn);
//plgn é um elemento de Polygon.
//Retorna o X equivalente ao vértice
float getYPlgn(Vertex plgn);
//plgn é um elemento de Polygon
//Retorna o Y equivalente ao vértice

#endif
