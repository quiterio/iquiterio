#ifndef BLCK__H
#define BLCK__H

#include <stdio.h>
#include "hash.h"
#include "arvore.h"
#include "city.h"

typedef void* Block;
/*Modela a quadra de uma cidade, sua representação é bastante semelhante a de um retângulo(Rect),porem a quadra não apresenta cores de a
preenchimento e borda, e diferentemente do retângulo que possuium identificador numérico a quadra possui um CEP alfanumérico*/
Block crtBlck(float pX, float pY, float pW, float pH, char *pId, char pType);
/*pW e pH que representam respectivamente a largura e altura da quadra devem servalores positivos*/

void chckBlckFace(char *id, HashTable blocks,List *lPlgn, FILE *outTxt);
void chckBlckHlf(char *id, HashTable blocks, List *lPlgn, FILE *outTxt);
void chckBlck(char *id, HashTable blocks,List *lPlgn, FILE *outTxt);
void wrtBlck(FILE *arq, Node pNode);
void vrfcBlcksQetCodTp(float x1, float y1, float x2, float y2, FILE *arq, City city, char* cod);
void vrfcBlcksR(Node pNode, float x1, float y1, float x2, float y2, FILE *arq, City city, int *o, HashTable (*getHash) (City),  void (*escreve) (FILE *, List, City));
void vrfcBlcksC (Node pNode, float x, float y, float r, FILE *arq, City city, int *o, HashTable (*getHash) (City),  void (*escreve) (FILE *, List, City));
char* getIdBlock(Block bl);
float getXBlck(Block bl);
//Retorna o valor de X da quadra(Block)
float getYBlck(Block bl);
//Retorna o valor de Y da quadra(Block)
float getWBlck(Block bl);
//Retorna o valor de W da quadra(Block)
float getHBlck(Block bl);
//Retorna o valor de H da quadra(Block)
int compTreeBlock(Block bl1, Block bl2);

void checkBlockXRelaph(Node pNode, float x1, float y1, float x2, float y2, City city);
void dstrsBlck(Block blk);

#endif
