#ifndef  SORT_H
#define SORT_H
#include <stdlib.h>
#include "list.h"
#include "arvore.h"
#include "city.h"

void kEstCom(char *cod, int k, float x, float y, City city, FILE *arq);
Item RdioBsClsr(City city, List *lSHP, float pX, float pY, int k, char o);
void RdioBsDist(City city, List *lSHP, float pX, float pY, int k, char o, FILE *arq, int n);
void RdioBsIntrvl(City city, List *lSHP, float pX, float pY, int k1, int k2, char o, FILE *arq, int n);
void HydrntClsr(City city, List *lSHP, float pX, float pY, int k, char o, FILE *arq, int n);
void HydrntDist(City city, List *lSHP, float pX, float pY, int k, char o, FILE *arq, int n);
void HydrntIntrvl(City city, List *lSHP, float pX, float pY, int k1, int k2, char o, FILE *arq, int n);
float distanceManhattan(float x1, float y1, float x2, float y2);
float distanceCartesian(float x1, float y1, float x2, float y2);
Item crtRltvEqp(char *str, float dis);
void crtCrclPntsClsr(List *lSHP, Item pCloser, City city, int end, float pX, float pY, char o, int n, FILE *arq);
char* getNameIRltvEqp(Item pRltvEqp, int i);


#endif
