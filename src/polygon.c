#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "list.h"
#include "equip.h"
#include "polygon.h"


typedef struct polygon{
  float xP, yP;
}Vrtx;

List creatPol(FILE *inptPol){
  float x, y;
  List *lPlygn;
  lPlygn = createList();

  while(feof(inptPol) != 1){
    fscanf(inptPol,"%f %f",&x,&y);
    addVrtx(lPlygn,x,y);
  }
  return lPlygn;
}

int runPolygon(List *lPlgn, float x1, float y1, float x2, float y2){
  int u;
  int i, j;
  float xP1, yP1, xP2, yP2;
  for(i=0;i<getLen(lPlgn);i++){
    if(i == (getLen(lPlgn)-1)) j = 0;
    else j = i+1;
    xP1 = getXPlgn(getItemN(lPlgn,i));
    yP1 = getYPlgn(getItemN(lPlgn,i));
    xP2 = getXPlgn(getItemN(lPlgn,j));
    yP2 = getYPlgn(getItemN(lPlgn,j));
    u = doIntersect(x1,y1,x2,y2,xP1,yP1,xP2,yP2);
    if(u == 1) return 1;
  }
  return 0;
}

int pointInPolygon(List *lPlgn, float x, float y)
{
  int i, j, c = 0;
  float xP1, xP2, yP1, yP2;
  for (i = 0, j = getLen(lPlgn)-1; i < getLen(lPlgn); j = i++) {
    yP1 = getYPlgn(getItemN(lPlgn,i));
    yP2 = getYPlgn(getItemN(lPlgn,j));
    xP1 = getXPlgn(getItemN(lPlgn,i));
    xP2 = getXPlgn(getItemN(lPlgn,j));
    if ( ((yP1>y) != (yP2>y)) &&
	 (x < (xP2-xP1) * (y-yP1) / (yP2-yP1) + xP1) )
       c = !c;
  }
  return c;
}

int doIntersect(float x1, float y1, float x2, float y2, float xP1, float yP1, float xP2, float yP2)
{
  int o1 = orientation(x1,y1, x2,y2, xP1,yP1);
  int o2 = orientation(x1,y1, x2,y2, xP2,yP2);
  int o3 = orientation(xP1,yP1, xP2,yP2, x1,y1);
  int o4 = orientation(xP1,yP1, xP2,yP2, x2,y2);

  if (o1 != o2 && o3 != o4)
      return 1;

  if (o1 == 0 && (onSegment(x1,y1, xP1,yP1, x2,y2) == 1)) return 1;

  if (o2 == 0 && (onSegment(x1,y1, xP2,yP2, x2,y2) == 1)) return 1;

  if (o3 == 0 && (onSegment(xP1,yP1, x1,y1, xP2,yP2) == 1)) return 1;

  if (o4 == 0 && (onSegment(xP1,yP1, x2,y2, xP2,yP2) == 1)) return 1;

return 0;
}

int orientation(float x1, float y1, float x2, float y2, float x3, float y3)
{
  int val = (y2 - y1) * (x3 - x2) - (x2 - x1) * (y3 - y2);

  if (val == 0) return 0;

  return (val > 0)? 1: 2;
}

int onSegment(float x1, float y1, float x2, float y2, float x3, float y3)
{
  if (x2 <= fmax(x1, x3) && x2 >= fmin(x1, x3) && y2 <= fmax(y1, y3) && y2 >= fmin(y1, y3)) return 1;

  return 0;
}

void addVrtx(List *lPlgn, float x, float y){
  Vrtx *aux = (Vrtx*)malloc(sizeof(Vrtx));
  aux->xP = x;
  aux->yP = y;
  pushBack(lPlgn,aux);
}

float getXPlgn(Vertex plgn){
  Vrtx *aux = (Vrtx*)plgn;
  return aux->xP;
}

float getYPlgn(Vertex plgn){
  Vrtx *aux = (Vrtx*)plgn;
  return aux->yP;
}
