#ifndef DC_H_
#define DC_H_

#include <stdlib.h>
#include "list.h"
#include "city.h"

void dcMons(FILE *inpt, char **pathOut, City city, List *shapes);

#endif
