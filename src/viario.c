#include "block.h"
#include "city.h"
#include "comercio.h"
#include "equip.h"
#include "file.h"
#include "hash.h"
#include "list.h"
#include "pessoas.h"
#include "viario.h"
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct _aresta {
  char *nome;
  char *proximo;
  float comprimento;
  float tempo;
  char *direita;
  char *esquerda;

} rua;

typedef struct _vertice {
  float x;
  float y;
  char *id;
  List vizinhos;
  // custo, pai -- colocar no dijkstra

} cruzamento;

Vertice criaCruzamento(float xx, float yy, char *ids) {
  cruzamento *aux = malloc(sizeof(cruzamento));
  aux->id = malloc((strlen(ids)) * sizeof(char));
  aux->x = xx;
  aux->y = yy;
  strcpy(aux->id, ids);
  aux->vizinhos = createList();
  return aux;
}

Aresta criaRua(char *nome, char *vaipara, float comprimento, float tempo,
               char *direita, char *esquerda) {
  rua *aux = malloc(sizeof(rua));
  aux->nome = malloc(strlen(nome) * sizeof(char));
  aux->proximo = malloc(strlen(vaipara) * sizeof(char));
  aux->direita = malloc(strlen(direita) * sizeof(char));
  aux->esquerda = malloc(strlen(esquerda) * sizeof(char));
  aux->comprimento = comprimento;
  aux->tempo = tempo;
  strcpy(aux->nome, nome);
  strcpy(aux->proximo, vaipara);
  strcpy(aux->direita, direita);
  strcpy(aux->esquerda, esquerda);
  return aux;
}

void armazenaViario(FILE *arq, City city) {
  char id[20], nome[30], direita[20], esquerda[20], i[20], j[20], op[5];
  float x, y, comprimento, tempo;
  while (fscanf(arq, "%s", op) != EOF) {
    if (strcmp(op, "o") == 0) {

      fscanf(arq, "%f %f %s", &x, &y, id);
      Vertice aux = criaCruzamento(x, y, id);

      if (getCruzamentoHash(city) == NULL) {
        HashTable hash = crtHashTable(11527);
        setCruzamentoHash(city, hash);
      }
      insereHash(getCruzamentoHash(city), aux, id);
      // printf("%f %f %s\n", x, y, id);

      // cruzamento *novo = fndKey(getCruzamentoHash(city), "(15,51)");

    } else {
      fscanf(arq, "%s %s %f %f %s %s %s", i, j, &comprimento, &tempo, nome,
             direita, esquerda);
      Aresta aux = criaRua(nome, j, comprimento, tempo, direita, esquerda);
      if (getRuaHash(city) == NULL) {
        HashTable hash = crtHashTable(11527);
        setRuaHash(city, hash);
      }
      insereHash(getRuaHash(city), aux, nome);

      cruzamento *origem = fndKey(getCruzamentoHash(city), i);
      pushFront(&origem->vizinhos, aux);

      // List listaArestas = createList();
      // fndKeyList(getRuaHash(city), "Conego_Januario", &listaArestas);
      // for (int k = 0; k < getLen(listaArestas); k++) {
      //   rua *novo = getItemN(listaArestas, k);
      //   printf("nome:%s \n", novo->nome);
      // }
    }
  }
}

int IndtoInt(char *lido) {
  int soma;

  soma = ((lido[1]) - 48);
  if (((lido[2]) > 47) && ((lido[2]) < 58)) {
    soma = soma * 10;
    soma += ((lido[2]) - 48);
  }
  return soma;
}

char achaHorientacaoRua(char *ruaP, City city) {
  List listaArestas = createList();
  float coordenadas[50][2];
  fndKeyList(getRuaHash(city), ruaP, &listaArestas);
  for (int k = 0; k < getLen(listaArestas); k++) {
    rua *novo = getItemN(listaArestas, k);
    cruzamento *nvCruz = fndKey(getCruzamentoHash(city), novo->proximo);
    coordenadas[k][0] = nvCruz->x;
    coordenadas[k][1] = nvCruz->y;
    // printf("nome:%s x:%f y:%f\n", novo->nome, nvCruz->x, nvCruz->y);
    if (k != 0) {
      if (coordenadas[k - 1][0] != coordenadas[k][0]) {
        if (coordenadas[k - 1][0] > coordenadas[k][0])
          return 'h';
        return 'h';
      } else if (coordenadas[k - 1][1] != coordenadas[k][1]) {
        if (coordenadas[k - 1][1] > coordenadas[k][1])
          return 'V';
        return 'v';
      }
      return 'N';
    }
  }
  return 'N';
}

void armazenaCepFaceNum(float *registrador, char *cep, char face, int num,
                        City city) {
  float x = getXBlck(fndKey(getBlocksHash(city), cep));
  float y = getYBlck(fndKey(getBlocksHash(city), cep));

  switch (face) {
  case 'N':
    x = x + (float)num;
    break;
  case 'S':
    y += getHBlck(fndKey(getBlocksHash(city), cep));
    x += (float)num;
    break;
  case 'L':
    x += getWBlck(fndKey(getBlocksHash(city), cep));
    y += (float)num;
    break;
  case 'O':
    y += (float)num;
    break;
  }
  // printf("dentro da func x:%f y:%f\n", x, y);
  registrador[0] = x;
  registrador[1] = y;
}

void armazenaNome(float *registrador, char *nome, City city) {
  // test
  char cpf[30], cep[20], face0[3], face;
  int num;
  strcpy(cpf, getCpfPessoa(fndKey(getNomePessoasHash(city), nome)));
  strcpy(cep, getCepMorador(fndKey(getCpfMoradoresHash(city), cpf)));
  strcpy(face0, getFaceMorador(fndKey(getCpfMoradoresHash(city), cpf)));
  face = face0[0];
  num = getNumMorador(fndKey(getCpfMoradoresHash(city), cpf));

  // printf("cep:%s face:%c num%d\n", cep, face, num);
  armazenaCepFaceNum(registrador, cep, face, num, city);
}

void armazenaFone(float *registrador, char *fone, City city) {
  char cpf[30], nome[20];

  strcpy(cpf, getCpfMorador(fndKey(getFoneMoradoresHash(city), fone)));
  strcpy(nome, getNomePessoa(fndKey(getCpfPessoaslHash(city), cpf)));

  armazenaNome(registrador, nome, city);
}

void escreveComercioRua(char *ruaP, char dir, char esq, City city, FILE *outTxt,
                        char *tp) {
  List listaArestas = createList();
  List naRua = createList();

  fndKeyList(getRuaHash(city), ruaP, &listaArestas);
  for (int k = 0; k < getLen(listaArestas); k++) {
    List ListaComercioCep1 = createList();
    List ListaComercioCep2 = createList();
    rua *aux = getItemN(listaArestas, k);
    // printf("%s %s\n", aux->direita, aux->esquerda);
    fndKeyList(getCepComercialHash(city), aux->direita, &ListaComercioCep1);
    fndKeyList(getCepComercialHash(city), aux->esquerda, &ListaComercioCep2);

    for (int l = 0; l < getLen(ListaComercioCep1); l++) {
      if ((getFaceEstCom(getItemN(ListaComercioCep1, l)) == dir) &&
          (strcmp(getCodTpEstCom(getItemN(ListaComercioCep1, l)), tp)) == 0) {
        pushFront(&naRua, getItemN(ListaComercioCep1, l));
      }
    }

    for (int m = 0; m < getLen(ListaComercioCep2); m++) {
      if ((getFaceEstCom(getItemN(ListaComercioCep2, m)) == esq) &&
          (strcmp(getCodTpEstCom(getItemN(ListaComercioCep2, m)), tp)) == 0) {
        pushFront(&naRua, getItemN(ListaComercioCep2, m));
      }
    }
  }
  if (getLen(naRua) > 0) {
    escreveNomeEndDes(outTxt, naRua, city);
  } else {
    fprintf(outTxt,
            "não foi encontrado nenhum estabelecimento comercial do "
            "tipo escolhido em uma das faces adjancentes à rua selecionada\n");
  }
}

void ConsultasViario(FILE *arq, char **pathOut, City city) {
  char consulta[50], id[20], strIndex[20], nomerua[50], tp[20], hrnt, ladoE,
      ladoD, cep[20], face, nome[50], fone[30];
  float registrador[32][2];
  int index, num;
  while (fscanf(arq, "%s", consulta) != EOF) {
    // printf("consulta %s\n", consulta);
    if (strcmp(consulta, "@f?") == 0) {
      fscanf(arq, "%s %s", strIndex, fone);
      index = IndtoInt(strIndex);
      armazenaFone(registrador[index], fone, city);
      // printf("f: x:%f y:%f\n", registrador[index][0], registrador[index][1]);
    } else if (strcmp(consulta, "@n?") == 0) {
      fscanf(arq, "%s %s", strIndex, nome);
      index = IndtoInt(strIndex);
      armazenaNome(registrador[index], nome, city); // test

    } else if (strcmp(consulta, "@e?") == 0) {
      fscanf(arq, "%s %s %c %d", strIndex, cep, &face, &num);
      index = IndtoInt(strIndex);
      armazenaCepFaceNum(registrador[index], cep, face, num, city);
      // printf("e: x:%f y:%f\n", registrador[index][0], registrador[index][1]);

    } else if (strcmp(consulta, "@g?") == 0) {
      fscanf(arq, "%s %s", strIndex, id);
      index = IndtoInt(strIndex);
      Equip *equip = fndKey(getEquipsHash(city), id);
      if (equip != NULL) {
        registrador[index][0] = getXEqp(equip);
        registrador[index][1] = getYEqp(equip);

        // cruzamento *aux = fndVrtcPerto(x, y, city);
      }
      // printf("g: x:%f y:%f\n", registrador[index][0], registrador[index][1]);
    } else if (strcmp(consulta, "p?") == 0) {
      /* code */
    } else if (strcmp(consulta, "pet?") == 0) {
      /* code */
    } else if (strcmp(consulta, "qer?") == 0) {
      fscanf(arq, "%s %s", nomerua, tp);
      FILE *outTxt = CrtFileTxt(pathOut);
      if (outTxt == NULL)
        printf("Problemas com o arquivo de saida %s comando qer?\n", *pathOut);
      else {
        fprintf(outTxt, "\n%s %s %s\n", consulta, nomerua, tp);
        // vrfcBlcksCodTp(outTxt, cod, city);
        hrnt = achaHorientacaoRua(nomerua, city);

        if (hrnt != 'N') {
          switch (hrnt) {
          case 'V':
            ladoD = 'O';
            ladoE = 'L';
            break;
          case 'v':
            ladoD = 'L';
            ladoE = 'O';
            break;
          case 'H':
            ladoD = 'N';
            ladoE = 'S';
            break;
          case 'h':
            ladoD = 'S';
            ladoE = 'N';
            break;
          }
        }

        escreveComercioRua(nomerua, ladoD, ladoE, city, outTxt, tp);
      }
    }
  }
}

// Vertice fndVrtcPerto(float x, float y, City city) {}
