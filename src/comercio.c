#include "block.h"
#include "city.h"
#include "comercio.h"
#include "hash.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct _tipoComercio {
  char *codTipo;
  char *descricao;
} tpCmrc;

typedef struct _estComercial {
  char *cnpj;
  char *codTipo;
  char *nome;
  char *cep;
  char face;
  int numero;
} EstCmrcl;

void arqTpEst(FILE *arq, City city) {
  char tp[10], des[50];
  while (fscanf(arq, "%s", tp) != EOF) {
    fscanf(arq, "%s", des);
    Comercio aux = criarTpCmrc(tp, des);
    if (getTipoComercioTree(city) == NULL) {
      Tree root = crtTree(&compTreeCodTp, &dstrsTipoComercio);
      setTipoComercioTree(city, root);
    }
    addNode(getTipoComercioTree(city), aux);
  }
}

void arqEstCom(FILE *arq, City city) {
  char cnpj[30], cod[10], nome[50], cep[50], face;
  int num;
  while (fscanf(arq, "%s", cnpj) != EOF) {
    fscanf(arq, "%s %s %s %c %d", cod, nome, cep, &face, &num);
    Comercio aux = criarEstblcmnt(cnpj, cod, nome, cep, face, num);
    if (getEstComercialTree(city) == NULL) {
      Tree root = crtTree(&compTreeComercio, &dstrsComercio);
      setEstComercialTree(city, root);
    }
    addNode(getEstComercialTree(city), aux);
    if (getCepComercialHash(city) == NULL) {
      HashTable hash = crtHashTable(1000);
      setCepComercialHash(city, hash);
    }
    insereHash(getCepComercialHash(city), aux, cep);
    if (getCodTpEstComHash(city) == NULL) {
      HashTable hash = crtHashTable(100);
      setCodTpEstComHash(city, hash);
    }
    insereHash(getCodTpEstComHash(city), aux, cod);
  }
}

Comercio criarEstblcmnt(char *cnpj, char *cod, char *nome, char *cep, char face,
                        int num) {
  EstCmrcl *aux = malloc(sizeof(EstCmrcl));
  aux->cnpj = malloc((strlen(cnpj) + 1) * sizeof(char));
  aux->codTipo = malloc((strlen(cod) + 1) * sizeof(char));
  aux->nome = malloc((strlen(nome) + 1) * sizeof(char));
  aux->cep = malloc((strlen(cep) + 1) * sizeof(char));
  strcpy(aux->cnpj, cnpj);
  strcpy(aux->codTipo, cod);
  strcpy(aux->nome, nome);
  strcpy(aux->cep, cep);
  aux->face = face;
  aux->numero = num;
  return aux;
}

Comercio criarTpCmrc(char *tp, char *des) {
  tpCmrc *aux = malloc(sizeof(tpCmrc));
  aux->codTipo = malloc((strlen(tp) + 1) * sizeof(char));
  aux->descricao = malloc((strlen(des) + 1) * sizeof(char));
  strcpy(aux->codTipo, tp);
  strcpy(aux->descricao, des);
  return aux;
}

int compTreeCodTp(Comercio comercio1, Comercio comercio2) {
  tpCmrc *aux1 = (tpCmrc *)comercio1;
  tpCmrc *aux2 = (tpCmrc *)comercio2;
  if (strcmp(aux1->codTipo, aux2->codTipo) > 0)
    return 1;
  else if (strcmp(aux1->codTipo, aux2->codTipo) < 0)
    return -1;
  else if (strcmp(aux1->codTipo, aux2->codTipo) == 0)
    return 0;
}

int compTreeComercio(Comercio comercio1, Comercio comercio2) {
  EstCmrcl *aux1 = (EstCmrcl *)comercio1;
  EstCmrcl *aux2 = (EstCmrcl *)comercio2;
  if (strcmp(aux1->cnpj, aux2->cnpj) > 0)
    return 1;
  else if (strcmp(aux1->cnpj, aux2->cnpj) < 0)
    return -1;
  else if (strcmp(aux1->cnpj, aux2->cnpj) == 0)
    return 0;
}

char *getEnderecoComercio(Comercio comercio) {
  EstCmrcl *aux = (EstCmrcl *)comercio;
  if (aux != NULL) {
    char *endereco = malloc((strlen(aux->cep) + 6) * sizeof(char));
    sprintf(endereco, "%s %c %d", aux->cep, aux->face, aux->numero);
    return endereco;
  }
  return NULL;
}

char *getNomeComercio(Comercio comercio) {
  EstCmrcl *aux = (EstCmrcl *)comercio;
  if (aux != NULL)
    return aux->nome;
  return NULL;
}

char *getCepEstCom(Comercio comercio) {
  EstCmrcl *aux = (EstCmrcl *)comercio;
  if (aux != NULL)
    return aux->cep;
  return NULL;
}

char *getCodTpEstCom(Comercio comercio) {
  EstCmrcl *aux = (EstCmrcl *)comercio;
  if (aux != NULL)
    return aux->codTipo;
  return NULL;
}

char *getDescricao(Comercio comercio) {
  tpCmrc *aux = (tpCmrc *)comercio;
  if (aux != NULL)
    return aux->descricao;
  return NULL;
}

int getNmrEstCom(Comercio comercio) {
  EstCmrcl *aux = (EstCmrcl *)comercio;
  if (aux != NULL)
    return aux->numero;
  return -1;
}

char getFaceEstCom(Comercio comercio) {
  EstCmrcl *aux = (EstCmrcl *)comercio;
  if (aux != NULL)
    return aux->face;
  return 'v';
}

char *getCNPJEstCom(Comercio comercio) {
  EstCmrcl *aux = (EstCmrcl *)comercio;
  if (aux != NULL)
    return aux->cnpj;
  return "erro";
}

void vrfcBlcksCodTp(FILE *outTxt, char *cod, City city) {
  List listaEstCom = createList();
  fndKeyList(getCodTpEstComHash(city), cod, &listaEstCom);
  if (getLen(listaEstCom) == 0)
    fprintf(outTxt, "Não existe estabelecimentos comercias desse tipo\n");
  escreveNomeEndDes(outTxt, listaEstCom, city);
}

void escreveNomeEndDes(FILE *arq, List listaEstCom, City city) {
  for (int i = 0; i < getLen(listaEstCom); i++) {
    EstCmrcl *comercio = getItemN(listaEstCom, i);
    char *endereco = getEnderecoComercio(comercio);
    tpCmrc *com = criarTpCmrc(comercio->codTipo, "nada");
    tpCmrc *tipo = getNode(getTipoComercioTree(city), com);
    fprintf(arq, "%s %s %s\n", comercio->nome, endereco, tipo->descricao);
  }
}
// void printComercio(Comercio comercio1){
//   EstCmrcl *aux = (EstCmrcl*)comercio1;
//   if(aux != NULL) printf("%s %s %s %s %c
//   %d\n",aux->cnpj,aux->codTipo,aux->nome,aux->cep,aux->face,aux->numero);
// }

void dstrsComercio(Comercio comercio) {
  EstCmrcl *aux = (EstCmrcl *)comercio;
  if (aux != NULL) {
    free(aux->cnpj);
    free(aux->nome);
    free(aux->cep);
    free(aux->codTipo);
    free(aux);
  }
}

void dstrsTipoComercio(Comercio comercio) {
  tpCmrc *aux = (tpCmrc *)comercio;
  if (aux != NULL) {
    free(aux->descricao);
    free(aux->codTipo);
    free(aux);
  }
}
