#ifndef FILE__H
#define FILE__H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


FILE* findInpt(int argc, char *argv[], char *str);
char* findPathInpt(int argc, char *argv[]);
char* findPathOut(int argc, char *argv[]);
FILE* CrtFileTxt(char **pathOut);
FILE* OpenFileTxt(char **path, char **pol);
FILE* CrtFileSVG(char **pathOut, char **sffx);

#endif
