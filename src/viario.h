#ifndef VIARIO__H
#define  VIARIO__H

#include <stdio.h>
#include "city.h"

typedef void *Aresta;
typedef void *Vertice;

void armazenaViario(FILE *arq, City city);
void ConsultasViario(FILE *arq, char **pathOut, City city);
float getXCruzamento(Vertice cruz);

#endif
