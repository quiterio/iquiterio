#ifndef LIST_H
#define LIST_H

typedef void* List;
typedef void* Item;

/*Modela uma lista, lista é um conglomerado de itens que podem ser acessado conforme a sua disposição na lista.
A lista possui um cabeçalho que indica qual o primeiro e ultimo item (caso existam) e tambémseu comprimento.*/

List createList();
//Cria e retorna um lista(List) de comprimento zero
void pushFront(List *i, Item obj);
//Insere o item obj na primeira posição da lista(List), e incrementa o valor de seu tamanho
void pushBack(List *i, Item obj);
//Insere o item obj na ultima posição da lista(List), e incrementa o valor de seutamanho
Item getItemN(List i, int n);
//O numero inteiro n deve ser maior ou igual a zero e menor ou igual ao tamanho dalista(List)
//Retorna o item de posição n na lista(List)
int getLen(List *i);
//Retorna a quantidade de elementos da lista(List)
void popFront(List i);
//A lista(List) não pode estar vazia
//Remove o primeiro elemento da lista(List)
void popBack(List i);
//A lista(List) não pode estar vazia
//Remove o ultimo elemento da lista(List)
void freeAll(List *i);
//Remove todos elementos da lista(List)
int itsEmpty(List i);
//Retorna 1 caso a lista(List) esteja vazia, e 0 caso exista algum item
void popItemN(List *lista, int n);

#endif
