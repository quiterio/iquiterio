#ifndef CITY_H_
#define CITY_H_

#include "arvore.h"
#include "hash.h"

typedef void* City;

City crtCity();

void setNomePessoasHash(City city, HashTable nomes);
void setRuaHash(City city, HashTable arestas);
void setCruzamentoHash(City city, HashTable cruzamentos);
void setBlocksHash(City city, HashTable blocks);
void setEquipsHash(City city, HashTable equips);
void setCepComercialHash(City city, HashTable cepCom);
void setCpfPessoaslHash(City city, HashTable cpfPess);
void setCepMoradoresHash(City city, HashTable cepMor);
void setCpfMoradoresHash(City city, HashTable cpfMor);
void setFoneMoradoresHash(City city, HashTable foneMor);
void setCodTpEstComHash(City city, HashTable pCodTpEstComHash);
void setCelPessoaHash(City city, HashTable pCelPessoaHash);
void setRdioBsCelularHash(City city, HashTable pRadioBsCelularHash);
void setCelRdioBsHash(City city, HashTable pCelRdioBsHash);
void setCelularHash(City city, HashTable pCelularHash);
void setBlcksTree(City city, Tree root);
void setHdrntTree(City city, Tree root);
void setTrffcLghtTree(City city, Tree root);
void setRdBsTree(City city, Tree root);
void setEstComercialTree(City city, Tree root);
void setTipoComercioTree(City city, Tree root);
void setPessoasTree(City city, Tree root);
void setMoradoresTree(City city, Tree root);

HashTable getRuaHash(City city);
HashTable getCruzamentoHash(City city);
HashTable getBlocksHash(City city);
HashTable getEquipsHash(City city);
HashTable getCepComercialHash(City city);
HashTable getCpfPessoaslHash(City city);
HashTable getNomePessoasHash(City city);
HashTable getCepMoradoresHash(City city);
HashTable getCpfMoradoresHash(City city);
HashTable getCodTpEstComHash(City city);
HashTable getFoneMoradoresHash(City city);
HashTable getCelPessoaHash(City city);
HashTable getRdioBsCelularHash(City city);
HashTable getCelRdioBsHash(City city);
HashTable getCelularHash(City city);
Tree getBlcksTree(City city);
Tree getHdrntTree(City city);
Tree getTrffcLghtTree(City city);
Tree getRdBsTree(City city);
Tree getEstComercialTree(City city);
Tree getTipoComercioTree(City city);
Tree getPessoasTree(City city);
Tree getMoradoresTree(City city);


#endif
