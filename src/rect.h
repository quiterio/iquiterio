#ifndef RCT__H
#define RCT__H
#include "list.h"

typedef void* Rectangle;
/*Modela a forma geométrica retângulo. Um retângulo é representado pelas coordenadas de seu pontoinferior esquerdo,
comprimento, largura, cor de preenchimento e de borda, sendo essas cores as validasdefinidas pelo padrão SVG*/
Rectangle crtRect(float pX, float pY, float pW, float pH, int pId, char clrFill[20],char clrStroke[20], char pType);
/*As variáveis pW(largura) e pH(altura) devem conter um valor maior que zero; Asvariáveis clrFill e clrStroke devem receber uma
String que contenha o nome ou código hexadecimalde uma cor valida definida pelo padrão SVG*/
/*Retorna um retangulo(Rect) com seu ponto inferior pX e pY, largura pW, alturapH, cor de preenchimento clrFill e de borda clrStroke*/
void createOvlrpRect(float xx,float yy,float ww, float hh, List shp);
void cmpRect(float x, float y, float w, float h, char *c1, char *c2, List *shp);
void chngInptClrRect(char *newFll, char *newStrk, char *fill, char *stroke);
void slctShpCmpR(float x, float y,float w,float h, char *c1, char *c2, List *shp, int i, int *n);

void chngClrR(Rectangle rect, char *newcolor);
//A variável newcolor deve receber uma String que contenha o nome ou código hexa-decimal de uma cor valida definida pelo padrão SVG
//Muda a cor do retângulo(Rect) para a cor recebida em newcolor
float getXr(Rectangle rect);
//Retorna o valor de X do retangulo(Rect)
float getYr(Rectangle rect);
//Retorna o valor de Y do retangulo(Rect)
float getWr(Rectangle rect);
//Retorna o valor de W do retangulo(Rect)
float getHr(Rectangle rect);
//Retorna o valor de H do retangulo(Rect)
char* getFllR(Rectangle rect);
//Retorna o nome ou código da cor de preenchimento usada no retângulo(Rect)
char* getStrkR(Rectangle rect);
//Retorna o nome ou código da cor de borda usada no retângulo(Rect)
int cmpRectTree(Rectangle rect1, Rectangle rect2);
#endif
