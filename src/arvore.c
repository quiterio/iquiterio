#include <stdio.h>
#include <stdlib.h>
#include "arvore.h"
#include "list.h"
#include "equip.h"
#include "sort.h"

typedef struct _Node{
	Data data;
	int balance;
	struct _Node *left;
  struct _Node *right;
  struct _Node *father;
}Nd;

typedef struct _Tree{
  Node head;
  int qtd;
  int (*fComp) (Data,Data);
	void (*fDestr) (Data);
}Rt;

Nd* crtNode(Data pData);
void blncTreeInsrt(Tree pTree, Node pNode, int balance);
void blncRmvTree(Tree pTree, Node pNode, int balance);
void rplcTree(Node pNode1, Node pNode2);
Nd* rttLeft(Tree pTree, Node pNode);
Nd* rttRight(Tree pTree, Node pNode);
Nd* rttLeftRight(Tree pTree, Node pNode);
Nd* rttRightLeft(Tree pTree, Node pNode);
int max(int a, int b);
Node fndData(Data pData, Node pAux, int (*funCompare) (Node,Node));

Data infoNo(Node pNode){
	Nd *aux = (Nd*)pNode;
	if(aux != NULL) return aux->data;
	return NULL;
}

Tree crtTree(int (*comp) (Data,Data), void (*dest) (Data)){
  Rt* aux = (Rt*) malloc(sizeof(Rt));
  aux->head = NULL;
  aux->qtd = 0;
  aux->fComp = comp;
	aux->fDestr = dest;
  return aux;
}

void addNode(Tree pTree, Data pData){
  Rt *root = (Rt*) pTree;
  Nd *node, *left, *right;

  root->qtd++;
	Nd* aux = crtNode(pData);
  if(root->head == NULL) root->head = aux;
  else{
    node = (Nd*) root->head;
    int (*comp) (Data,Data) = root->fComp;
    while (node != NULL){
      if(comp(pData,node->data) < 0){
        left = node->left;
        if(left == NULL){
          node->left = aux;
					aux->father = node;
          blncTreeInsrt(pTree,node,-1);
          break;
        }
        else node = left;
      }
      else if(comp(pData,node->data) > 0){
        right = node->right;
        if(right == NULL){
          node->right = aux;
					aux->father = node;
          blncTreeInsrt(pTree,node,1);
          break;
        }
        else node = right;
      }
      else{
        node->data = pData;
        break;
      }
    }
  }
}

Nd* crtNode(Data pData){
	Nd *aux = (Nd*) malloc(sizeof(Nd));
	aux->data = pData;
  aux->balance = 0;
  aux->left = NULL;
  aux->right = NULL;
  aux->father = NULL;
	return aux;
}

void rmvNode(Tree pTree, Node pNode){
  if(pTree != NULL && pNode != NULL){

    Nd *node = (Nd*) pNode;
    Nd *left = node->left;
    Nd *right = node->right;
    Nd *toDelete = node;

    Rt *root = (Rt*) pTree;;
    if(left == NULL){
      if(right == NULL){
        if(node == root->head) root->head = NULL;
        else{
          Nd* father = node->father;
          if(father->left == node){
            father->left = NULL;
            blncRmvTree(root,father,1);
          }
          else{
            father->right = NULL;
            blncRmvTree(root,father,-1);
          }
        }
      }
      else{
        rplcTree (node, right);
        blncRmvTree (root, node, 0);
        toDelete = right;
      }
    }
		else if(right == NULL){
      rplcTree (node, left);
      blncRmvTree (root, node, 0);
      toDelete = left;
    }
    else{
      Nd *aux = right;
      if(aux->left == NULL){
        Nd *father = node->father;
        aux->father = father;
        aux->left = left;
        aux->balance = node->balance;

        if(left != NULL) left->father = aux;
        if(node == root->head) root->head = aux;
        else{
          if(father->left == node) father->left = aux;
          else father->right = aux;
        }
        blncRmvTree (root, aux, -1);
      }
      else{
        while(aux->left != NULL){
          aux = aux->left;
        }
        Nd *father = node->father;
        Nd *auxParent = aux->father;
        Nd *auxRight = aux->right;

        if (auxParent->left == aux) auxParent->left = auxRight;
        else auxParent->right = auxRight;

        if (auxRight != NULL) auxRight->father = auxParent;

        aux->father = father;
        aux->left = left;
        aux->balance = node->balance;
        aux->right = right;
        right->father = aux;

        if (left != NULL) left->father = aux;
        if (node == root->head) root->head = aux;
        else{
          if(father->left == node) father->left = aux;
          else father->right = aux;
        }
        blncRmvTree (root, auxParent, 1);
      }
    }
    root->qtd--;
    free(toDelete);
  }
}

void blncTreeInsrt(Tree pTree, Node pNode, int balance){
  Rt *root = (Rt*) pTree;
  Nd *node, *father;

  node = (Nd*) pNode;
  while(node != NULL){
    node->balance += balance;
    balance = node->balance;
    if(balance == 0) break;
    else if(balance == -2){
      if (node->left->balance == -1) rttRight(root,node);
      else rttLeftRight(root, node);
      break;
    }
    else if(balance == 2){
      if(node->right->balance == 1) rttLeft(root, node);
      else rttRightLeft(root, node);
      break;
    }
    father = node->father;
    if(father != NULL) balance = (father->left == node) ? -1 : 1;
    node = father;
  }
}

void blncRmvTree(Tree pTree, Node pNode, int balance){
  Rt *root = (Rt*) pTree;
  Nd *node = (Nd*) pNode;

  while(node != NULL){
    node->balance += balance;
    balance = node->balance;

    if(balance == -2){
      if(node->left->balance <= 0){
        node = rttRight(root,node);

        if (node->balance == 1) break;
      }
      else node = rttLeftRight(root,node);
    }
    else if(balance == 2){
      if(node->right->balance >= 0){
        node = rttLeft(root,node);
        if(node->balance == -1) break;
      }
      else node = rttRightLeft(root, node);
    }
    else if(balance != 0) break;
    Nd* father = node->father;
    if(father != NULL) balance = (father->left == node) ? 1 : -1;
    node = father;
  }
}

void rplcTree(Node pNode1, Node pNode2){
  Nd *node1 = (Nd*) pNode1;
  Nd *node2 = (Nd*) pNode2;
  Nd *left = node2->left;
  Nd *right = node2->right;

  node1->balance = node2->balance;
  node1->data = node2->data;
  node1->left = left;
  node1->right = right;

  if(left != NULL) left->father = node1;
  if (right != NULL) right->father = node1;
}

Nd* rttLeft(Tree pTree, Node pNode){
  Nd *node = (Nd*) pNode;
  Nd *right = node->right;
  Nd *rightLeft = right->left;
  Nd *father = node->father;
  Rt *root = (Rt*) pTree;

  right->father = father;
  right->left = node;
  node->right = rightLeft;
  node->father = right;

  if(rightLeft != NULL) rightLeft->father = node;
  if (node == root->head) root->head = right;
  else if(father->right == node) father->right = right;
  else father->left = right;

  right->balance--;
  node->balance = -right->balance;

  return right;
}

Nd* rttRight(Tree pTree, Node pNode){
  Nd *node = (Nd*) pNode;
  Nd *left = node->left;
  Nd *leftRight = left->right;
  Nd *father = node->father;
  Rt *root = (Rt*) pTree;

  left->father = father;
  left->right = node;
  node->left = leftRight;
  node->father = left;

  if(leftRight != NULL) leftRight->father = node;
  if(node == root->head) root->head = left;
  else if(father->left == node) father->left = left;
  else father->right = left;

  left->balance++;
  node->balance = -left->balance;

  return left;
}

Nd* rttLeftRight(Tree pTree, Node pNode){
  Nd *node = (Nd*) pNode;
  Nd *left = node->left;
  Nd *leftRight = left->right;
  Nd *father = node->father;
  Nd *leftRightRight = leftRight->right;
  Nd *leftRightLeft = leftRight->left;
  Rt *root = (Rt*) pTree;

  leftRight->father = father;
  node->left = leftRightRight;
  left->right = leftRightLeft;
  leftRight->left = left;
  leftRight->right = node;
  left->father = leftRight;
  node->father = leftRight;

  if(leftRightRight != NULL) leftRightRight->father = node;

  if(leftRightLeft != NULL) leftRightLeft->father = left;

  if(node == root->head) root->head = leftRight;
  else if(father->left == node) father->left = leftRight;
  else father->right = leftRight;

  if(leftRight->balance == 1){
    node->balance = 0;
    left->balance = -1;
  }
  else if(leftRight->balance == 0){
    node->balance = 0;
    left->balance = 0;
  }
  else{
    node->balance = 1;
    left->balance = 0;
  }

  leftRight->balance = 0;

  return leftRight;
}

Nd* rttRightLeft(Tree pTree, Node pNode){
  Nd *node = (Nd*) pNode;
  Nd *right = node->right;
  Nd *rightLeft = right->left;
  Nd *father = node->father;
  Nd *rightLeftLeft = rightLeft->left;
  Nd *rightLeftRight = rightLeft->right;
  Rt *root = (Rt*) pTree;

  rightLeft->father = father;
  node->right = rightLeftLeft;
  right->left = rightLeftRight;
  rightLeft->right = right;
  rightLeft->left = node;
  right->father = rightLeft;
  node->father = rightLeft;

  if(rightLeftLeft != NULL) rightLeftLeft->father = node;
  if(rightLeftRight != NULL) rightLeftRight->father = right;

  if(node == root->head) root->head = rightLeft;
  else if(father->right == node) father->right = rightLeft;
  else father->left = rightLeft;

  if(rightLeft->balance == -1){
    node->balance = 0;
    right->balance = 1;
  }
  else if(rightLeft->balance == 0){
    node->balance = 0;
    right->balance = 0;
  }
  else{
    node->balance = -1;
    right->balance = 0;
  }

  rightLeft->balance = 0;

  return rightLeft;
}

int max(int a, int b){
  return a > b ? a : b;
}

Node getRight(Node pNode){
  Nd* aux = (Nd*)pNode;
  if(aux != NULL) return aux->right;
  return NULL;
}

Node getLeft(Node pNode){
  Nd* aux = (Nd*)pNode;
  if(aux != NULL) return aux->left;
  return NULL;
}

int getQtd(Tree pTree){
  Rt* aux = (Rt*)pTree;
  if(aux != NULL) return aux->qtd;
  return 0;
}

Node getHead(Tree pTree){
  Rt* aux = (Rt*) pTree;
  if(aux != NULL) return aux->head;
  return NULL;
}

void getDistancesTree(Tree pNode, float pX, float pY, List *pntsOn, float (distance) (float,float,float,float)){
  Nd *nod = (Nd*)pNode;
  if(nod != NULL){
    float x = getXEqp(nod->data);
    float y = getYEqp(nod->data);
    float dis = distance(pX,pY,x,y);
    Item aux = crtRltvEqp(getIdEqp(nod->data),dis);
    pushFront(pntsOn,aux);
    getDistancesTree(nod->left,pX,pY,pntsOn,distance);
    getDistancesTree(nod->right,pX,pY,pntsOn,distance);
  }
}

Node getNode(Tree pTree, Data data){
  Rt *root = (Rt*)pTree;
  if(root != NULL && root->head != NULL){
    Nd *aux = fndData(data,root->head,root->fComp);
    return aux->data;
  }
  return NULL;
}

Node fndData(Data pData, Node pAux, int (*funCompare) (Node,Node)){
  Nd *aux = (Nd*)pAux;
  if(aux == NULL){
    printf("Valor não encontrado\n");
    return aux;
  }
  if(funCompare(pData,aux->data) == 0) return aux;
  if(funCompare(pData,aux->data) > 0) return fndData(pData,aux->right,funCompare);
  if(funCompare(pData,aux->data) < 0) return fndData(pData,aux->left,funCompare);
}

void rmvData(Data pData, Tree pTree){
  Rt *root = (Rt*)pTree;
  if(root != NULL && root->head != NULL){
    Nd *aux = fndData(pData,root->head,root->fComp);
    if(aux != NULL) rmvNode(root,aux);
  }
}
