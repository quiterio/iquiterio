#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"
#include "circle.h"
#include "rect.h"
#include "shape.h"



typedef struct Circl{
  char type;
  int id;
  float x, y, r;
  char fill[20], stroke[20];
}Crcl;

Circle crtCrcl(float pX, float pY, float pR, char clrFill[20], char clrStroke[20], int pId, char pType){
  Crcl *aux = (Crcl*)malloc(sizeof(Crcl));
  aux->x = pX;
  aux->y = pY;
  aux->r = pR;
  aux->id = pId;
  strcpy(aux->fill,clrFill);
  strcpy(aux->stroke,clrStroke);
  aux->type = pType;
  return (Circle) aux;
}

void chngInptClrCirc(char *newFll, char *newStrk, char *fill, char *stroke)
{
  if(strcmp(newFll,"-") == 0) strcpy(newFll,"none");
  if(strcmp(newStrk,"-") == 0) strcpy(newStrk,"none");

  strcpy(fill,newFll);
  strcpy(stroke,newStrk);
}

void cmpCirc(float x, float y, float r, char *c1, char *c2, List *shp){
  int n, i , itsIn;
  char type;
  n = 0;
  for(i=0;i<getLen(*shp);i++){
    slctShpCmpC(x,y,r,c1,c2,shp,i,&n);
  }
  printf("comparacoes xc = %d\n",n);
}

void slctShpCmpC(float x, float y,float r, char *c1, char *c2, List *shp, int i, int *n){
  char color[20], type;
  int itsIn;
  itsIn = 0;
  type = getTypeShp(getItemN(*shp,i));
  if(type == 'c'){
    strcpy(color,getFllC(getItemN(*shp,i)));
    if (strcmp(c1,color) == 0 || strcmp(c1,"*") == 0){
      itsIn = inCC(x,y,r, getItemN(*shp,i));
      if (itsIn == 1) chngClrC(getItemN(*shp,i), c2);
    }
    *n += 1;
  }else if(type == 'r'){
    strcpy(color,getFllR(getItemN(*shp,i)));
    if (strcmp(c1,color) == 0 || strcmp(c1,"*") == 0){
      if (itsIn == 1) chngClrR(getItemN(*shp,i), c2);
      itsIn = inCR(x,y,r, getItemN(*shp,i));
    }
    *n += 1;
  }
}

void chngClrC(Circle circl, char *newcolor){
  Crcl *aux = (Crcl*) circl;
  strcpy(aux->fill,newcolor);
}

float getXc(Circle circl){
  Crcl *aux = (Crcl*) circl;
  return aux->x;
}

float getYc(Circle circl){
  Crcl *aux = (Crcl*) circl;
  return aux->y;
}


float getRc(Circle circl){
  Crcl *aux = (Crcl*) circl;
  return aux->r;
}

char* getFllC(Circle circl){
  Crcl *aux = (Crcl*) circl;
  return aux->fill;
}


char* getStrkC(Circle circl){
  Crcl *aux = (Crcl*) circl;
  return aux->stroke;
}
