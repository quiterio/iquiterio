#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "list.h"
#include "shape.h"
#include "rect.h"
#include "circle.h"
#include "block.h"

typedef struct Shap{
  char type;
  int id;
  float x, y;
}Shp;

int getIdShp(Shape shap){
  Shp *aux = (Shp*) shap;
  return aux->id;
}

char getTypeShp(Shape rect){
  Shp *aux = (Shp*) rect;
  return aux->type;
}

void overlap(int id1, int id2, List *shp){
  char type1, type2;
  int ovrlp;
  Rectangle *aux;
  if(getItemIdShp(*shp,id1) != NULL && getItemIdShp(*shp,id2) != NULL){
    type1 = getTypeShp(getItemIdShp(*shp,id1));
    type2 = getTypeShp(getItemIdShp(*shp,id2));

    if (type1 == 'r' && type2 == 'r') {
      ovrlp = overlapRR(id1,id2,*shp);
      if (ovrlp == 1){
        aux = fndPntsRR(id1,id2,*shp);
        pushBack(*shp,aux);
      }
    }else if(type1 == 'c' && type2 == 'c'){
      ovrlp = overlapCC(id1,id2,*shp);
      if (ovrlp == 1) {
        aux = fndPntsCC(id1,id2,*shp);
        pushBack(*shp,aux);
      }
    }else if((type1 == 'r' && type2 == 'c') || (type1 == 'c' && type2 == 'r')){
      if (type1 == 'c') ovrlp = overlapCR(id1,id2,*shp);
      else if(type2 == 'c') ovrlp = overlapCR(id2,id1,*shp);
      if (ovrlp == 1 && type1 == 'c'){
        aux = fndPntsCR(id1,id2,*shp);
        pushBack(*shp,aux);
      }
      else if(ovrlp == 1 && type2 == 'c'){
        aux = fndPntsCR(id2,id1,*shp);
        pushBack(*shp,aux);
      }
    }
  }
}


int overlapRR(int id1, int id2, List shp){
  float x1, x2, y1, y2, w1, w2, h1, h2;

  x1 = getXr(getItemIdShp(shp,id1));
  x2 = getXr(getItemIdShp(shp,id2));
  y1 = getYr(getItemIdShp(shp,id1));
  y2 = getYr(getItemIdShp(shp,id2));
  w1 = getWr(getItemIdShp(shp,id1));
  w2 = getWr(getItemIdShp(shp,id2));
  h1 = getHr(getItemIdShp(shp,id1));
  h2 = getHr(getItemIdShp(shp,id2));

  if((x1+w1 < x2) || (x2+w2 < x1) || (y1+h1 < y2) || (y2+h2 < y1)) return 0;
  return 1;
}

int overlapCC(int id1, int id2, List shp){
  float x1, x2, y1, y2, r1, r2;

  x1 = getXc(getItemIdShp(shp,id1));
  x2 = getXc(getItemIdShp(shp,id2));
  y1 = getYc(getItemIdShp(shp,id1));
  y2 = getYc(getItemIdShp(shp,id2));
  r1 = getRc(getItemIdShp(shp,id1));
  r2 = getRc(getItemIdShp(shp,id2));

  if((r1 + r2) >= distanceTo(x1,y1,x2,y2)) return 1;
  return 0;
}

int overlapCR(int cir, int rtc, List shp){
  float xC, yC, rC, xR, yR, wR, hR, distx, disty, corner;

  xC = getXc(getItemIdShp(shp,cir));
  yC = getYc(getItemIdShp(shp,cir));
  rC = getRc(getItemIdShp(shp,cir));
  xR = getXr(getItemIdShp(shp,rtc));
  yR = getYr(getItemIdShp(shp,rtc));
  wR = getWr(getItemIdShp(shp,rtc));
  hR = getHr(getItemIdShp(shp,rtc));

  xR += wR/2;
  yR += hR/2;
  distx = abs(xC - xR);
  disty = abs(yC - yR);

  if ((distx > (wR/2 + rC)) || (disty > (hR/2 + rC))) return 0;
  if ((distx <= (wR/2)) || (disty <= (hR/2)) ) return 1;

  corner = pow((distx - wR/2),2) + pow((disty - hR/2),2);

  if (corner <= pow(rC,2)) return 1;

}

float distanceTo(float x1, float y1, float x2, float y2){
 float difx, dify, dist;
 difx = x2-x1;
 dify = y2-y1;
 dist = pow(pow(difx,2) + pow(dify,2),(0.5));
 return dist;
}


Rectangle fndPntsRR(int id1, int id2, List shp){
  float x1, x2, y1, y2, w1, w2, h1, h2, mx, Mx, my, My;
  Rectangle aux;

  x1 = getXr(getItemIdShp(shp,id1));
  x2 = getXr(getItemIdShp(shp,id2));
  y1 = getYr(getItemIdShp(shp,id1));
  y2 = getYr(getItemIdShp(shp,id2));
  w1 = getWr(getItemIdShp(shp,id1));
  w2 = getWr(getItemIdShp(shp,id2));
  h1 = getHr(getItemIdShp(shp,id1));
  h2 = getHr(getItemIdShp(shp,id2));

  //encontra menor x
  if (x1 < x2) mx = x1;
    else       mx = x2;
  //encontra maior X
  if ((x1 + w1) > (x2 + w2)) Mx = (x1 + w1);
    else                     Mx = (x2 + w2);
  //encontra menor y
  if (y1 < y2) my = y1;
    else       my = y2;
  //encontra maior y
  if ((y1 + h1) > (y2 + h2)) My = (y1 + h1);
    else                     My = (y2 + h2);

  aux = crtRect(mx,my,(Mx-mx),(My-my),-1,"none","pink",'s');
  return (Rectangle) aux;
}

Rectangle fndPntsCC(int id1, int id2, List shp){
  float x1, x2, y1, y2, r1, r2, mx, Mx, my, My;
  Rectangle aux;

  x1 = getXc(getItemIdShp(shp,id1));
  x2 = getXc(getItemIdShp(shp,id2));
  y1 = getYc(getItemIdShp(shp,id1));
  y2 = getYc(getItemIdShp(shp,id2));
  r1 = getRc(getItemIdShp(shp,id1));
  r2 = getRc(getItemIdShp(shp,id2));

  //encontra menor x
  if (x1 - r1 <  x2 - r2) mx = (x1 - r1);
   else                   mx = (x2 - r2);
  //encontra maior x
  if (x1 + r1 >  x2 + r2) Mx = (x1 + r1);
   else                   Mx = (x2 + r2);
  //encontra menor y
  if (y1 - r1 <  y2 - r2) my = (y1 - r1);
   else                   my = (y2 - r2);
  //encontra maior y
  if (y1 + r1 >  y2 + r2) My = (y1 + r1);
   else                   My = (y2 + r2);

  aux = crtRect(mx,my,(Mx-mx),(My-my),-1,"none","pink",'s');
  return (Rectangle) aux;
}

Rectangle fndPntsCR(int id1, int id2, List shp){
  float xC, yC, rC, xR, yR, wR, hR, mx, Mx, my, My;
  Rectangle aux;

  xC = getXc(getItemIdShp(shp,id1));
  yC = getYc(getItemIdShp(shp,id1));
  rC = getRc(getItemIdShp(shp,id1));
  xR = getXr(getItemIdShp(shp,id2));
  yR = getYr(getItemIdShp(shp,id2));
  wR = getWr(getItemIdShp(shp,id2));
  hR = getHr(getItemIdShp(shp,id2));

  //encontra o menor x
  if (xC - rC < xR) mx = (xC - rC);
   else             mx = xR;
  // encontra o maior x
  if (xC + rC > xR + wR) Mx = (xC + rC);
   else                  Mx = (xR + wR);
  //encontra o menor y
  if (yC - rC < yR) my = (yC - rC);
   else             my = yR;
  //encontra o maior y
  if (yC + rC > yR + wR) My = (yC + rC);
   else                  My = (yR + wR);

  aux = crtRect(mx,my,(Mx-mx),(My-my),-1,"none","pink",'s');
  return (Rectangle) aux;
}

int inCC(float Bx, float By, float Br, Circle circl){
  float Sx, Sy, Sr, dist;

  Sx=getXc(circl);
  Sy=getYc(circl);
  Sr=getRc(circl);

  dist = distanceTo(Bx,By, Sx, Sy);
  if(Br >= (dist + Sr)) return 1;
  return 0;
}

int inCR(float Cx, float Cy, float r, Rectangle rect){
  float Rx, Ry, w, h, dist1, dist2, dist3, dist4;

  Rx = getXr(rect);
  Ry = getYr(rect);
  w = getWr(rect);
  h = getHr(rect);

  dist1 = distanceTo(Cx,Cy,Rx,Ry);
  dist2 = distanceTo(Cx,Cy,Rx,Ry+h);
  dist3 = distanceTo(Cx,Cy,Rx+w,Ry);
  dist4 = distanceTo(Cx,Cy,Rx+w,Ry+h);

  if ((r >= dist1) && (r >= dist2) && (r >= dist3) && (r >= dist4)) return 1;
  return 0;
}

int inRC(float Bx, float By, float Bw,float Bh, Circle circl){
  float Cx, Cy, r, Sx, Sy, Sw, Sh;

  Cx = getXc(circl);
  Cy = getYc(circl);
  r = getRc(circl);

  Sx = Cx - r;
  Sy = Cy - r;
  Sw = Sx + 2*r;
  Sh = Sy + 2*r;

  if ((Bx <= Sx) && (By <= Sy) && ((Bx+Bw) >= (Sx+Sw)) && ((By+Bh) >= (Sy+Sh))) return 1;
  return 0;
}

int inRR(float Bx, float By, float Bw,float Bh, Rectangle rect){
  float Sx, Sy, Sw, Sh;

  Sx = getXr(rect);
  Sy = getYr(rect);
  Sw = getWr(rect);
  Sh = getHr(rect);

  if ((Bx <= Sx) && (By <= Sy) && ((Bx+Bw) >= (Sx+Sw)) && ((By+Bh) >= (Sy+Sh))) return 1;
  return 0;
}

Shape getItemIdShp(List *lSHP, int id){
  int i, searchId;
  for(i=0;i<getLen(lSHP);i++){
    searchId = getIdShp(getItemN(lSHP,i));
    if(searchId == id) return getItemN(lSHP,i);
  }
  return NULL;
}
