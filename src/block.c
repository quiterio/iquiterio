#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "block.h"
#include "list.h"
#include "arvore.h"
#include "pessoas.h"
#include "comercio.h"
#include "cro.h"
#include "polygon.h"
#include "city.h"
#include "shape.h"
#include "equip.h"

typedef struct _blck{
  char type;
  char id[50];
  float x, y, w, h;
}Blck;

void chckBlckFace(char *id, HashTable blocks,List *lPlgn, FILE *outTxt){
  float x, y, w, h;
  int v1, v2, v3, v4;

  Blck* aux = fndKey(blocks,id);

  if(aux != NULL){
    x = aux->x;
    y = aux->y;
    w = aux->w;
    h = aux->h;

    v1 = pointInPolygon(lPlgn,x,y);
    v2 = pointInPolygon(lPlgn,x+w,y);
    v3 = pointInPolygon(lPlgn,x,y+h);
    v4 = pointInPolygon(lPlgn,x+w,y+h);

    if((v1 == 0 || v2 == 0 || runPolygon(lPlgn,x,y,x+w,y) == 1)
      && (v1 == 0 || v3 == 0 || runPolygon(lPlgn,x,y,x,y+h) == 1)
        && (v2 == 0 || v4 == 0 || runPolygon(lPlgn,x+w,y,x+w,y+h) == 1)
          && (v3 == 0 || v4 == 0 || runPolygon(lPlgn,x,y+h,x+w,y+h) == 1))
            fprintf(outTxt,"Não há face inteiramente dentro\n");
    else{
      if(v1 == 1 && v2 == 1 && runPolygon(lPlgn,x,y,x+w,y) == 0) fprintf(outTxt,"Face sul\n");
      if(v1 == 1 && v3 == 1 && runPolygon(lPlgn,x,y,x,y+h) == 0) fprintf(outTxt,"Face leste\n");
      if(v2 == 1 && v4 == 1 && runPolygon(lPlgn,x+w,y,x+w,y+h) == 0) fprintf(outTxt,"Face oeste\n");
      if(v3 == 1 && v4 == 1 && runPolygon(lPlgn,x,y+h,x+w,y+h) == 0) fprintf(outTxt,"Face norte\n");
    }

    fprintf(outTxt,"\n");
  }
  else fprintf(outTxt,"Quadra com CEP inexistente\n");
}

void chckBlckHlf(char *id, HashTable blocks, List *lPlgn, FILE *outTxt){
  float x, y, w, h;

  Blck* aux = fndKey(blocks,id);

  if(aux != NULL){
    x = aux->x;
    y = aux->y;
    w = aux->w;
    h = aux->h;

    if(runPolygon(lPlgn,x,y,x+w,y) == 0 && runPolygon(lPlgn,x,y,x,y+h) == 0 && runPolygon(lPlgn,x+w,y,x+w,y+h) == 0 && runPolygon(lPlgn,x,y+h,x+w,y+h) == 0)
      fprintf(outTxt,"Não há face parcialmente dentro\n");
    else{
      if(runPolygon(lPlgn,x,y,x+w,y) == 1) fprintf(outTxt,"Face sul parcialmente dentro\n");
      if(runPolygon(lPlgn,x,y,x,y+h) == 1) fprintf(outTxt,"Face leste parcialmente dentro\n");
      if(runPolygon(lPlgn,x+w,y,x+w,y+h) == 1) fprintf(outTxt,"Face oeste parcialmente dentro\n");
      if(runPolygon(lPlgn,x,y+h,x+w,y+h) == 1) fprintf(outTxt,"Face norte parcialmente dentro\n");
    }
    fprintf(outTxt,"\n");
  }
  else fprintf(outTxt,"Quadra com CEP inexistente\n");
}

void chckBlck(char *id, HashTable blocks,List *lPlgn, FILE *outTxt){
  float x, y, w, h;
  int v1, v2, v3, v4;
  int u1, u2, u3, u4;

  Blck *aux = fndKey(blocks,id);

  if(aux != NULL){
    x = aux->x;
    y = aux->y;
    w = aux->w;
    h = aux->h;

    v1 = pointInPolygon(lPlgn,x,y);
    v2 = pointInPolygon(lPlgn,x+w,y);
    v3 = pointInPolygon(lPlgn,x,y+h);
    v4 = pointInPolygon(lPlgn,x+w,y+h);


    if(v1 == 1 && v2 == 1 && v3 == 1 && v4 == 1){
      u1 = runPolygon(lPlgn,x,y,x+w,y);
      u2 = runPolygon(lPlgn,x,y,x,y+h);
      u3 = runPolygon(lPlgn,x,y+h,x+w,y+h);
      u4 = runPolygon(lPlgn,x+w,y,x+w,y+h);
      if(u1 == 1 || u2 == 1 || u3 == 1 || u4 == 1) fprintf(outTxt,"Quadra fora\n");
      else fprintf(outTxt,"Quadra dentro\n");
    }
    else fprintf(outTxt,"Quadra fora\n");
    fprintf(outTxt,"\n");
  }
  else fprintf(outTxt,"Quadra com CEP inexistente\n");
}

void wrtBlck(FILE *arq, Node pNode){
  if(pNode != NULL){
    Blck *aux = (Blck*)infoNo(pNode);
    float x = aux->x;
    float y = aux->y;
    float w = aux->w;
    float h = aux->h;
    char id[50];
    strcpy(id,aux->id);
    fprintf(arq,"\t<rect x=\"%.2f\" y=\"%.2f\" width=\"%.2f\" height=\"%.2f\" fill=\"skyblue\" stroke=\"blue\" fill-opacity=\"0.8\" />\n",x,y,w,h);
    fprintf(arq,"<text text-anchor=\"middle\" x=\"%.2f\" y=\"%.2f\" fill=\"white\">%s</text>\n",x+w/2,y+h/2,id);
    wrtBlck(arq,getLeft(pNode));
    wrtBlck(arq,getRight(pNode));
  }
}

void vrfcBlcksQetCodTp(float x1, float y1, float x2, float y2, FILE *arq, City city, char* cod){
  List listaEstCom = createList();
  List listaEstComDentro = createList();
  fndKeyList(getCodTpEstComHash(city),cod,&listaEstCom);
  for(int i=0;i<getLen(listaEstCom);i++){
    Comercio estCom = getItemN(listaEstCom,i);
    Blck *bl = fndKey(getBlocksHash(city),getCepEstCom(estCom));
    if(bl != NULL && bl->x >= x1 && bl->x <= x2 && bl->y >= y1 && bl->y <= y2) pushFront(&listaEstComDentro,estCom);
  }
  escreveNomeEndDes(arq,listaEstComDentro,city);
  if(getLen(listaEstCom) == 0) fprintf(arq,"Não existe nenhum estabelecimento comercial desse tipo\n");
  else if(getLen(listaEstComDentro) == 0) fprintf(arq,"Nenhuma quadra com esse tipo de estabelecimento comercial dentro da região\n");
}

void vrfcBlcksR (Node pNode, float x1, float y1, float x2, float y2, FILE *arq, City city, int *o, HashTable (*getHash) (City),  void (*escreve) (FILE *, List, City)){
  Blck *aux = NULL;
  if(pNode != NULL) aux = (Blck*)infoNo(pNode);
  if(aux != NULL){
    if(aux->x < x1) vrfcBlcksR(getRight(pNode),x1,y1,x2,y2,arq,city,o, getHash, escreve);
    else if((aux->x + aux->w) > x2) vrfcBlcksR(getLeft(pNode),x1,y1,x2,y2,arq,city,o, getHash, escreve);
    else if((aux->y + aux->h) <= y2 && aux->y >= y1){
      List lista = createList();
      fndKeyList(getHash(city),aux->id,&lista);
      *o = 1;
      escreve(arq,lista,city);
      vrfcBlcksR(getLeft(pNode),x1,y1,x2,y2,arq,city,o, getHash, escreve);
      vrfcBlcksR(getRight(pNode),x1,y1,x2,y2,arq,city,o, getHash, escreve);
    }
    else {
      vrfcBlcksR(getLeft(pNode),x1,y1,x2,y2,arq,city,o, getHash, escreve);
      vrfcBlcksR(getRight(pNode),x1,y1,x2,y2,arq,city,o, getHash, escreve);
    }
  }
}

void vrfcBlcksC (Node pNode, float x, float y, float r, FILE *arq, City city, int *o, HashTable (*getHash) (City),  void (*escreve) (FILE *, List, City)){
  Blck *aux = NULL;
  if(pNode != NULL) aux = (Blck*)infoNo(pNode);
  if(aux != NULL){
    if(aux->x < x-r) vrfcBlcksC(getRight(pNode),x,y,r,arq,city,o, getHash, escreve);
    else if((aux->x + aux->w) > x+r) vrfcBlcksC(getLeft(pNode),x,y,r,arq,city,o, getHash, escreve);
    else if(distanceTo(x,y,aux->x,aux->y) <= r &&
            distanceTo(x,y,(aux->x + aux->w),aux->y) <= r &&
            distanceTo(x,y,(aux->x + aux->w),(aux->y + aux->h)) <= r &&
            distanceTo(x,y,(aux->x),(aux->y + aux->h)) <= r){
      List lista = createList();
      fndKeyList(getHash(city),aux->id,&lista);
      *o = 1;
      escreve(arq,lista,city);
      vrfcBlcksC(getLeft(pNode),x,y,r,arq,city,o, getHash, escreve);
      vrfcBlcksC(getRight(pNode),x,y,r,arq,city,o, getHash, escreve);
    }
    else {
      vrfcBlcksC(getLeft(pNode),x,y,r,arq,city,o, getHash, escreve);
      vrfcBlcksC(getRight(pNode),x,y,r,arq,city,o, getHash, escreve);
    }
  }
}

Block crtBlck(float pX, float pY, float pW, float pH, char *pId, char pType)
{
  Blck *aux = malloc(sizeof(Blck));
  aux->x = pX;
  aux->y = pY;
  aux->w = pW;
  aux->h = pH;
  aux->type = pType;
  strcpy(aux->id,pId);
  return (Block) aux;
}

char* getIdBlock(Block bl){
  Blck *aux = (Blck*) bl;
  if(aux != NULL) return aux->id;
  return "erro";
}

float getXBlck(Block bl)
{
  Blck *aux = (Blck*) bl;
  if(aux != NULL) return aux->x;
  return -1;
}

float getYBlck(Block bl)
{
  Blck *aux = (Blck*) bl;
  if(aux != NULL) return aux->y;
  return -1;
}

float getWBlck(Block bl)
{
  Blck *aux = (Blck*) bl;
  if(aux != NULL) return aux->w;
  return -1;
}

float getHBlck(Block bl)
{
  Blck *aux = (Blck*) bl;
  if(aux != NULL) return aux->h;
  return -1;
}

int compTreeBlock(Block bl1, Block bl2){
  Blck *aux1 = (Blck*) bl1;
  Blck *aux2 = (Blck*) bl2;
  if(aux1->x == aux2->x && aux1->y == aux2->y) return 0;
  if(aux1->x > aux2->x) return 1;
  else if(aux1->x < aux2->x) return -1;
  else if(aux1->y > aux2->y) return 2;
  else if(aux1->y < aux2->y) return -2;
}

void checkBlockXRelaph(Node pNode, float x1, float y1, float x2, float y2, City city){
  if(pNode != NULL){
    Blck *aux = (Blck*) infoNo(pNode);
    checkBlockXRelaph(getLeft(pNode),x1,y1,x2,y2,city);
    checkBlockXRelaph(getRight(pNode),x1,y1,x2,y2,city);
    if(aux->x < x1) checkBlockXRelaph(getRight(pNode),x1,y1,x2,y2,city);
    else if(aux->x + aux->w > x2) checkBlockXRelaph(getLeft(pNode),x1,y1,x2,y2,city);
    else{
      if(aux->y >= y1 && aux->y+aux->h <= y2 && aux->x >= x1 && aux->x+aux->w <= x2){
        int o = 0;
        if(getHdrntTree(city) != NULL) equipXRelaphHydra(getHead(getHdrntTree(city)),city,aux->x-10,aux->y-10,aux->x+aux->w+10,aux->y+10+aux->h,&o);
        if(o == 1){
          // remove morador
          Item morador = fndKey(getCepMoradoresHash(city),aux->id);
          if(morador != NULL){
            dltKeyHash(getCepMoradoresHash(city),aux->id);
            rmvData(morador,getMoradoresTree(city));
          }
          // remove estabelecimeto comercial
          Item comercio = fndKey(getCepComercialHash(city),aux->id);
          if(comercio != NULL){
            dltKeyHash(getCepComercialHash(city),aux->id);
            dltKeyListHash(getCodTpEstComHash(city),getCodTpEstCom(comercio),comercio,&compTreeComercio);
            rmvData(comercio,getEstComercialTree(city));
          }
          // remove equips
          if(getRdBsTree(city) != NULL) equipXRelaphRdBs(getHead(getRdBsTree(city)),city,aux->x,aux->y,aux->x+aux->w,aux->y+aux->h);
          if(getTrffcLghtTree(city) != NULL) equipXRelaphTrffcLght(getHead(getTrffcLghtTree(city)),city,aux->x-30,aux->y-30,aux->x+aux->w,aux->y+aux->h);
          // remove block
          dltKeyHash(getBlocksHash(city),aux->id);
          rmvData(aux,getBlcksTree(city));
        }
      }
    }
  }
}

void dstrsBlck(Block blk){
  Blck *aux = (Blck*) blk;
  if(aux != NULL){
    free(aux);
  }
}
