#include "file.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *findName(char **orgnlNmWthPth);

FILE *findInpt(int argc, char *argv[], char *str) {
  FILE *arq1 = NULL;
  int i;
  for (i = 0; i < argc; i++) {
    if (strcmp(argv[i], str) == 0)
      arq1 = fopen(argv[++i], "r");
  }
  return arq1;
}

char *findPathInpt(int argc, char *argv[]) {
  int i;
  char *fTerm;
  for (i = 0; i < argc; i++) {
    if (strcmp(argv[i], "-f") == 0) {
      fTerm = (char *)malloc((strlen(argv[++i]) + 1) * sizeof(char));
      strcpy(fTerm, argv[i]);
    }
  }
  for (i = strlen(fTerm); i >= 0; i--) {
    if (fTerm[i] == '/')
      break;
  }
  fTerm[++i] = '\0';
  return fTerm;
}

char *findPathOut(int argc, char *argv[]) {
  int i;
  char *fTerm, *oTerm, *path, *name;
  for (i = 0; i < argc; i++) {
    if (strcmp(argv[i], "-f") == 0) {
      fTerm = (char *)malloc((strlen(argv[++i]) + 1) * sizeof(char));
      strcpy(fTerm, argv[i]);
    }
    if (strcmp(argv[i], "-o") == 0) {
      oTerm = (char *)malloc((strlen(argv[++i]) + 1) * sizeof(char));
      strcpy(oTerm, argv[i]);
    }
  }
  name = findName(&fTerm);
  path = (char *)malloc((strlen(name) + strlen(oTerm) + 2) * sizeof(char));
  strcpy(path, oTerm);
  strcat(path, "/");
  strcat(path, name);

  return path;
}

char *findName(char **orgnlNmWthPth) {
  char *str, *lastToken;
  lastToken = (char *)malloc((strlen(*orgnlNmWthPth) + 1) * sizeof(char));
  strcpy(lastToken, strtok(*orgnlNmWthPth, "/"));
  str = strtok(NULL, "/");
  while (str != NULL) {
    strcpy(lastToken, str);
    str = strtok(NULL, "/");
  }
  strcpy(lastToken, strtok(lastToken, "."));
  return lastToken;
}

FILE *CrtFileTxt(char **pathOut) {
  FILE *aux;
  char *outPath;
  outPath = malloc((strlen(*pathOut) + 10) * sizeof(char));
  strcpy(outPath, *pathOut);
  strcat(outPath, ".txt");
  aux = fopen(outPath, "a+t");
  return aux;
}

FILE *OpenFileTxt(char **path, char **pol) {
  char *pathOut;
  FILE *inptTxt;
  int i;
  char *auxPath = malloc((strlen(*path) + 2) * sizeof(char));
  strcpy(auxPath, *path);
  for (i = strlen(auxPath) + 1; i >= 0; i--) {
    if (auxPath[i] == '/')
      break;
  }

  auxPath[++i] = '\0';
  pathOut = (char *)malloc((strlen(auxPath) + strlen(*pol) + 5) * sizeof(char));
  strcpy(pathOut, auxPath);
  strcat(pathOut, *pol);
  inptTxt = fopen(pathOut, "r");

  return inptTxt;
}

FILE *CrtFileSVG(char **pathOut, char **sffx) {
  FILE *aux;
  char *outPath;
  outPath = malloc((strlen(*pathOut) + strlen(*sffx) + 10) * sizeof(char));
  strcpy(outPath, *pathOut);
  strcat(outPath, "-");
  strcat(outPath, *sffx);
  strcat(outPath, ".svg");
  aux = fopen(outPath, "w");
  return aux;
}
