#ifndef SVG__H
#define SVG__H
#include <stdlib.h>
#include "list.h"
#include "shape.h"
#include "equip.h"
#include "block.h"
#include "city.h"
//Modela arquivos de formato .svg. Respeitando o padrão SVG
// void crtSvg(char *sffx, char **path, List *blocks, List *lSHP, List *equips, int ctrl);
/*path deve ser um diretório válido, sffx não pode conter ’.’(ponto). Blocks, lShp eequips devem ser as listas respectivas
as listas de Blocks, de Shapes e Equip. Width e height maiorque zero. Ctrl deve ser 0 ou 1*/
/*Cria um arquivo svg respeitando o padrão SVG no diretório path com o nome doarquivo Cro mais sffx se ctrl igual a 1 e sem o sffx
se ctrl igual a 0. O arquivo svg criado contéma altura e a largura respectiva a height e width e quadras(Blocks) que estão na lista Blocks,
assim como circulos(Circle) e retangulos(Rect) da lista lShp e os equipamentos(Equip) da lista equips. */
void crtSvg(char **sffx, char **path, City city, List *lSHP, int ctrl);
void crtPrtl(char **sffx, char *color, float r, char **path, List *lSHP);

#endif
