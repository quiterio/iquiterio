#include "arvore.h"
#include "block.h"
#include "circle.h"
#include "city.h"
#include "comercio.h"
#include "equip.h"
#include "list.h"
#include "sort.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct elemendist {
  char name[50];
  float dist;
} rltvEqp;

typedef struct colors { char color[20]; } clr;

rltvEqp *crtVtrDstncEstCom(List listaEstCom, City city, float x, float y,
                           int *o, FILE *arq);
int findK(rltvEqp *pntsOn, int strt, int end, int total, int elmntPstn);
void QuickSort(rltvEqp *pntsOn, int strt, int end, int total);
float sortSubVet(rltvEqp *pntsOn, int strt, int end, int total);
rltvEqp findMedian(rltvEqp *pntsOn, int total);
int partitions(rltvEqp *pntsOn, int strt, int end, float pvt);
void InsertionSort(rltvEqp *pntsOn, int strt, int end);
void crtCrclPntsInt(City city, List *lSHP, rltvEqp *pntsOn, int result1,
                    int result2, float pX, float pY, char o, int n, FILE *arq);
void crtCrclPntsDist(City city, List *lSHP, rltvEqp *pntsOn, int result1,
                     int total, float pX, float pY, char o, int n, FILE *arq);
rltvEqp *listToArray(List pntsOn);

void kEstCom(char *cod, int k, float x, float y, City city, FILE *arq) {
  List listaEstCom = createList();
  fndKeyList(getCodTpEstComHash(city), cod, &listaEstCom);
  if (getLen(listaEstCom) > 0) {
    int o = 0;
    rltvEqp *vetorDistancia =
        crtVtrDstncEstCom(listaEstCom, city, x, y, &o, arq);
    if (o == 1) {
      if (k > getLen(listaEstCom))
        k = getLen(listaEstCom);
      int result =
          findK(vetorDistancia, 0, getLen(listaEstCom), getLen(listaEstCom), k);
      QuickSort(vetorDistancia, 0, k, k);
      for (int i = 0; i < k; i++) {
        if (vetorDistancia[i].dist != -1) {
          Comercio com =
              fndKey(getCepComercialHash(city), vetorDistancia[i].name);
          char *nome = getNomeComercio(com);
          char *endereco = getEnderecoComercio(com);
          fprintf(arq, "%s %s\n", nome, endereco);
        }
      }
    }
  } else
    fprintf(arq, "Não existe estabelecimento comercial desse tipo\n");
}

rltvEqp *crtVtrDstncEstCom(List listaEstCom, City city, float x, float y,
                           int *o, FILE *arq) {
  rltvEqp *vetorDistancia = malloc(getLen(listaEstCom) * sizeof(rltvEqp));
  for (int i = 0; i < getLen(listaEstCom); i++) {
    Comercio estCom = getItemN(listaEstCom, i);
    Block bl = fndKey(getBlocksHash(city), getCepEstCom(estCom));
    int numeroEst = getNmrEstCom(estCom);
    char face = getFaceEstCom(estCom);
    if (bl != NULL) {
      float xBlock = getXBlck(bl);
      float yBlock = getYBlck(bl);
      if (face == 'N') {
        xBlock += numeroEst;
        yBlock += getHBlck(bl);
      } else if (face == 'S')
        xBlock += numeroEst;
      else if (face == 'L')
        yBlock += numeroEst;
      else if (face == 'O') {
        xBlock += getWBlck(bl);
        yBlock += numeroEst;
      }
      float dis = distanceManhattan(x, y, xBlock, yBlock);
      *o = 1;
      strcpy(vetorDistancia[i].name, getCepEstCom(estCom));
      vetorDistancia[i].dist = dis;
    } else {
      fprintf(arq, "Quadra não encontrada %s\n", getCepEstCom(estCom));
      strcpy(vetorDistancia[i].name, "nada");
      vetorDistancia[i].dist = -1;
    }
  }
  return vetorDistancia;
}

Item RdioBsClsr(City city, List *lSHP, float pX, float pY, int k, char o) {
  List pntsOn = createList();
  int result;
  Tree radioBs = getRdBsTree(city);
  int RdioBsNmbr = getQtd(radioBs);
  getDistancesTree(getHead(radioBs), pX, pY, &pntsOn, &distanceCartesian);
  rltvEqp *closer = listToArray(pntsOn);
  if (k > RdioBsNmbr)
    k = RdioBsNmbr;
  InsertionSort(closer, 0, RdioBsNmbr);
  return closer;
}

void RdioBsDist(City city, List *lSHP, float pX, float pY, int k, char o,
                FILE *arq, int n) {
  List pntsOn = createList();
  Tree radioBs = getRdBsTree(city);
  int RdioBsNmbr, result;
  RdioBsNmbr = getQtd(radioBs);
  getDistancesTree(getHead(radioBs), pX, pY, pntsOn, &distanceCartesian);
  rltvEqp *closer = listToArray(pntsOn);
  if (k > RdioBsNmbr)
    k = RdioBsNmbr;
  result = findK(closer, 0, RdioBsNmbr, RdioBsNmbr, RdioBsNmbr - k);
  if (o == '@')
    QuickSort(closer, RdioBsNmbr - k, RdioBsNmbr, k);
  crtCrclPntsDist(city, lSHP, closer, result, RdioBsNmbr - 1, pX, pY, o, n,
                  arq);
}

void RdioBsIntrvl(City city, List *lSHP, float pX, float pY, int k1, int k2,
                  char o, FILE *arq, int n) {
  List pntsOn = createList();
  int i, RdioBsNmbr, result1, result2;
  Tree radioBs = getRdBsTree(city);
  RdioBsNmbr = getQtd(radioBs);
  getDistancesTree(getHead(radioBs), pX, pY, pntsOn, &distanceCartesian);
  rltvEqp *closer = listToArray(pntsOn);
  if (k1 <= RdioBsNmbr) {
    if (k2 > RdioBsNmbr)
      k2 = RdioBsNmbr;
    result1 = findK(closer, 0, RdioBsNmbr, RdioBsNmbr, k1);
    result2 =
        findK(closer, result1, RdioBsNmbr, RdioBsNmbr - result1, k2 - result1);
    if (o == '@')
      QuickSort(closer, result1, result2 + 1, (result2 + 1) - result1);
    crtCrclPntsInt(city, lSHP, closer, result1, result2 + result1, pX, pY, o, n,
                   arq);
  }
}

void HydrntClsr(City city, List *lSHP, float pX, float pY, int k, char o,
                FILE *arq, int n) {
  Tree hydrTree = getHdrntTree(city);
  List pntsOn = createList();
  int HdrntsNmbr, result;
  HdrntsNmbr = getQtd(hydrTree);
  getDistancesTree(getHead(hydrTree), pX, pY, pntsOn, &distanceManhattan);
  rltvEqp *closer = listToArray(pntsOn);
  if (k > HdrntsNmbr)
    k = HdrntsNmbr;
  result = findK(closer, 0, HdrntsNmbr, HdrntsNmbr, k);
  if (o == '@')
    QuickSort(closer, 0, k, k);
  crtCrclPntsClsr(lSHP, closer, city, result, pX, pY, o, n, arq);
}

void HydrntDist(City city, List *lSHP, float pX, float pY, int k, char o,
                FILE *arq, int n) {
  List pntsOn = createList();
  Tree hydrTree = getHdrntTree(city);
  int HdrntsNmbr, result;
  HdrntsNmbr = getQtd(hydrTree);
  getDistancesTree(getHead(hydrTree), pX, pY, pntsOn, &distanceManhattan);
  rltvEqp *closer = listToArray(pntsOn);
  if (k > HdrntsNmbr)
    k = HdrntsNmbr;
  result = findK(closer, 0, HdrntsNmbr, HdrntsNmbr, HdrntsNmbr - k);
  if (o == '@')
    QuickSort(closer, HdrntsNmbr - k, HdrntsNmbr, k);
  crtCrclPntsDist(city, lSHP, closer, result, HdrntsNmbr - 1, pX, pY, o, n,
                  arq);
}

void HydrntIntrvl(City city, List *lSHP, float pX, float pY, int k1, int k2,
                  char o, FILE *arq, int n) {
  List pntsOn = createList();
  int i, HdrntsNmbr, result1, result2;
  Tree hydrTree = getHdrntTree(city);
  HdrntsNmbr = getQtd(hydrTree);
  if (k1 <= HdrntsNmbr) {
    if (k2 > HdrntsNmbr)
      k2 = HdrntsNmbr;
    getDistancesTree(getHead(hydrTree), pX, pY, pntsOn, &distanceManhattan);
    rltvEqp *closer = listToArray(pntsOn);
    result1 = findK(closer, 0, HdrntsNmbr, HdrntsNmbr, k1);
    result2 =
        findK(closer, result1, HdrntsNmbr, HdrntsNmbr - result1, k2 - result1);
    if (o == '@')
      QuickSort(closer, result1, result2 + 1, (result2 + 1) - result1);
    crtCrclPntsInt(city, lSHP, closer, result1, result2 + result1, pX, pY, o, n,
                   arq);
  }
}

int findK(rltvEqp *pntsOn, int strt, int end, int total, int elmntPstn) {
  float pvt;
  int mddlElmntPstn, i;
  if (total <= 80) {
    InsertionSort(pntsOn, strt, end);
    return elmntPstn - 1;
  } else {
    pvt = sortSubVet(pntsOn, strt, end, total);
  }
  mddlElmntPstn = partitions(pntsOn, strt, end, pvt);
  if (elmntPstn <= mddlElmntPstn)
    findK(pntsOn, strt, mddlElmntPstn, mddlElmntPstn - strt, elmntPstn);
  else
    findK(pntsOn, mddlElmntPstn - 1, end, end - (mddlElmntPstn - 1), elmntPstn);
}

void QuickSort(rltvEqp *pntsOn, int strt, int end, int total) {
  float pvt;
  int mddlElmntPstn;
  if (total <= 80)
    InsertionSort(pntsOn, strt, end);
  else {
    pvt = sortSubVet(pntsOn, strt, end, total);
    mddlElmntPstn = partitions(pntsOn, strt, end, pvt);
    QuickSort(pntsOn, strt, mddlElmntPstn, mddlElmntPstn - strt);
    QuickSort(pntsOn, mddlElmntPstn - 1, end, end - (mddlElmntPstn - 1));
  }
}

float sortSubVet(rltvEqp *pntsOn, int strt, int end, int total) {
  int i, j, aux;
  rltvEqp subVetor[5], *mdns, medi1, medi2;
  float mdnOfMdn;
  mdns = (rltvEqp *)malloc(((total / 5) + 1) * sizeof(rltvEqp));
  i = 0;
  j = 0;
  while (strt < end) {
    subVetor[i] = pntsOn[strt];
    if (i == 4) {
      mdns[j] = findMedian(subVetor, 5);
      j++;
      i = -1;
    } else if (strt + 1 == end) {
      mdns[j] = findMedian(subVetor, i + 1);
      j++;
    }
    strt++;
    i++;
  }
  if (j % 2 == 0) {
    aux = j / 2;
    medi1 = mdns[findK(mdns, 0, j, j, aux)];
    medi2 = mdns[findK(mdns, 0, j, j, aux - 1)];
    mdnOfMdn = (medi1.dist + medi2.dist) / 2;
  } else {
    aux = (j - 1) / 2;
    mdnOfMdn = mdns[findK(mdns, 0, j, j, aux)].dist;
  }
  return mdnOfMdn;
}

rltvEqp findMedian(rltvEqp *pntsOn, int total) {
  int hlf;
  rltvEqp mediana;
  InsertionSort(pntsOn, 0, total);
  if (total % 2 == 1) {
    hlf = (total - 1) / 2;
    mediana = pntsOn[hlf];
  } else {
    hlf = total / 2;
    mediana.dist = (pntsOn[hlf].dist + pntsOn[hlf - 1].dist) / 2;
  }
  return mediana;
}

int partitions(rltvEqp *pntsOn, int strt, int end, float pvt) {
  int i, j;
  rltvEqp aux;
  i = strt;
  j = end - 1;
  while (i < j) {
    while (pntsOn[i].dist < pvt && i < j)
      i++;
    while (pntsOn[j].dist > pvt && j >= 0)
      j--;
    if (i < j) {
      aux = pntsOn[i];
      pntsOn[i] = pntsOn[j];
      pntsOn[j] = aux;
    }
  }
  for (i = 0; i <= end; i++) {
    if (pntsOn[i].dist > pvt)
      return i;
  }
}

void InsertionSort(rltvEqp *pntsOn, int strt, int end) {
  int i, j;
  rltvEqp elct;
  for (i = strt; i < end; i++) {
    elct = pntsOn[i];
    j = i - 1;
    while ((j >= strt) && (elct.dist < pntsOn[j].dist)) {
      pntsOn[j + 1] = pntsOn[j];
      j -= 1;
    }

    pntsOn[j + 1] = elct;
  }
}

float distanceManhattan(float x1, float y1, float x2, float y2) {
  float x, y;
  if (x1 >= x2)
    x = x1 - x2;
  else
    x = x2 - x1;
  if (y1 >= y2)
    y = y1 - y2;
  else
    y = y2 - y1;

  return x + y;
}

float distanceCartesian(float x1, float y1, float x2, float y2) {
  float x, y;
  x = x1 - x2;
  y = y1 - y2;
  x = powf(x, 2);
  y = powf(y, 2);
  return powf(x + y, 0.5);
}

float getXIdSort(char name[50], List *equips) {
  int i;
  for (i = 0; i < getLen(*equips); i++) {
    if (strcmp(name, getIdEqp(getItemN(*equips, i))) == 0)
      return getXEqp(getItemN(*equips, i));
  }
  return -1;
}

float getYIdSort(char name[50], List *equips) {
  int i;
  for (i = 0; i < getLen(*equips); i++) {
    if (strcmp(name, getIdEqp(getItemN(*equips, i))) == 0)
      return getYEqp(getItemN(*equips, i));
  }
  return -1;
}

void crtCrclPntsInt(City city, List *lSHP, rltvEqp *pntsOn, int result1,
                    int result2, float pX, float pY, char o, int n, FILE *arq) {
  int i, end, cont, random;
  HashTable hash = getEquipsHash(city);
  Circle pnt;
  clr colr[6];
  strcpy(colr[0].color, "#ffd42a");
  strcpy(colr[1].color, "#4aae4a");
  strcpy(colr[2].color, "0044aa");
  strcpy(colr[3].color, "#2a7fff");
  strcpy(colr[4].color, "#666666");
  strcpy(colr[5].color, "#000000");
  random = rand();
  random = random % 6;
  cont = 0;
  while (result1 <= result2) {
    cont++;
    Item aux = fndKey(hash, pntsOn[result1].name);
    if (o == '@')
      setPosEqp(aux, cont);
    else
      setPosEqp(aux, 0);
    fprintf(arq, "%s ", pntsOn[result1].name);
    setClrEqp(aux, colr[random].color);
    result1++;
  }
  fprintf(arq, "\n\n");
  pnt = crtCrcl(pX, pY, 0, colr[random].color, colr[random].color, n, 'p');
  pushFront(lSHP, pnt);
}

void crtCrclPntsDist(City city, List *lSHP, rltvEqp *pntsOn, int result1,
                     int total, float pX, float pY, char o, int n, FILE *arq) {
  int i, end, cont, random;
  HashTable hash = getEquipsHash(city);
  clr colr[6];
  Circle pnt;
  strcpy(colr[0].color, "#ffd42a");
  strcpy(colr[1].color, "#4aae4a");
  strcpy(colr[2].color, "0044aa");
  strcpy(colr[3].color, "#2a7fff");
  strcpy(colr[4].color, "#666666");
  strcpy(colr[5].color, "#000000");
  random = rand();
  random = random % 6;
  i = total;
  end = result1;
  cont = 0;
  while (i > end) {
    cont++;
    Item aux = fndKey(hash, pntsOn[i].name);
    if (o == '@')
      setPosEqp(aux, cont);
    else
      setPosEqp(aux, 0);
    setClrEqp(aux, colr[random].color);
    fprintf(arq, "%s ", pntsOn[i].name);
    i--;
  }
  fprintf(arq, "\n\n");
  pnt = crtCrcl(pX, pY, 0, colr[random].color, colr[random].color, n, 'p');
  pushFront(lSHP, pnt);
}

void crtCrclPntsClsr(List *lSHP, Item pCloser, City city, int end, float pX,
                     float pY, char o, int n, FILE *arq) {
  rltvEqp *closer = (rltvEqp *)pCloser;
  HashTable hash = getEquipsHash(city);
  int random, i;
  clr colr[6];
  Circle pnt;
  strcpy(colr[0].color, "#ffd42a");
  strcpy(colr[1].color, "#4aae4a");
  strcpy(colr[2].color, "0044aa");
  strcpy(colr[3].color, "#2a7fff");
  strcpy(colr[4].color, "#666666");
  strcpy(colr[5].color, "#000000");
  random = rand();
  random = random % 6;
  for (i = 0; i <= end; i++) {
    Item aux = fndKey(hash, closer[i].name);
    if (o == '@')
      setPosEqp(aux, i + 1);
    else
      setPosEqp(aux, 0);
    setClrEqp(aux, colr[random].color);
    fprintf(arq, "%s ", closer[i].name);
  }
  fprintf(arq, "\n\n");
  pnt = crtCrcl(pX, pY, 0, colr[random].color, colr[random].color, n, 'p');
  pushFront(lSHP, pnt);
}

Item crtRltvEqp(char *str, float dis) {
  rltvEqp *aux = malloc(sizeof(rltvEqp));
  aux->dist = dis;
  strcpy(aux->name, str);
  return aux;
}

rltvEqp *listToArray(List pntsOn) {
  int mnt = getLen(pntsOn);
  rltvEqp *aux = malloc((mnt + 2) * sizeof(rltvEqp));
  for (int i = 0; i < mnt; i++) {
    rltvEqp *it = getItemN(pntsOn, i);
    aux[i] = *it;
  }
  freeAll(pntsOn);
  return aux;
}

char *getNameIRltvEqp(Item pRltvEqp, int i) {
  rltvEqp *aux = (rltvEqp *)pRltvEqp;
  if (aux != NULL)
    return aux[i].name;
  return "erro";
}
