#ifndef CITY__H
#define CITY__H

#include "hash.h"
#include "arvore.h"
#include "city.h"
#include <stdio.h>

typedef void* Equip;
/*Modela equipamentos de uma cidade. Sendo eles do tipo hidrante, semáforo ou torre de celular.
Eles possuem coordenadas de sua localização e um identificador*/
void* crtEqp(float pX, float pY, char *pId, char type);
//O caractere type deve ser ’t’, ’d’ ou ’h’, sendo respectivamente semáforo, torre decelular e hidrante
//Retorna um equipamento(Equip) de coordenadas pX e pY, com identificador pId etipo type
void wrtTrafficLght(FILE *arq, Node pNode);
void wrtHydra(FILE *arq, Node pNode);
void wrtRdBs(FILE *arq, Node pNode);
float getXEqp(Equip elemnt);
//Retorna o valor de X do equipamento(Equip)
float getYEqp(Equip elemnt);
//Retorna o valot de Y do equipamento(Equip)
char getTypeEqp(Equip elemnt);
//Retorna o tipo do equipamento(Equip)
char* getIdEqp(Equip elemnt);
//Retorna o identificador do equipamento(Equip)
void chckEqp(char *id, HashTable equips, List *lPlgn, FILE *outTxt);
Equip getItemIdEqp(List *lEqp, char *id);
void setPosEqp(Equip elemnt, int p);
int getPosEqp(Equip elemnt);
void setClrEqp(Equip elemnt, char *c);
char* getClrEqp(Equip elemnt);
int compTreeEquip(Equip eqp1, Equip eqp2);
void relaphHydra(Node pNode, float x1, float y1, float x2, float y2, int *o, FILE *outTxt, char *comand, City city);
// void equipXRelaph(Node pNode, City city, float x1, float y1, float x2, float y2, Node (*getEquipTree) (City), int *o);
void equipXRelaphHydra(Node pNode, City city, float x1, float y1, float x2, float y2, int *o);
void equipXRelaphRdBs(Node pNode, City city, float x1, float y1, float x2, float y2);
void equipXRelaphTrffcLght(Node pNode, City city, float x1, float y1, float x2, float y2);
void rdBsBohma(FILE *outTxt, float x, float y, float r, Node pNode, int *o);
void procuraCelRdioBs(City city, FILE *outTxt, Node pNode);
void dstrsEquip(Equip eqp);

#endif
