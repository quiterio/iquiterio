#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dcmons.h"
#include "list.h"
#include "city.h"
#include "arvore.h"
#include "rect.h"
#include "circle.h"
#include "equip.h"
#include "file.h"
#include "block.h"

void relaphComand(FILE *inpt, char **pathOut, City city, List *shapes);
void xRelaphComand(FILE *inpt, char **pathOut, City city, List *shapes);
void bohmaComand(FILE *inpt, char **pathOut, City city, List *shapes);

void dcMons(FILE *inpt, char **pathOut, City city, List *shapes){
  char comand[10];

  while(fscanf(inpt,"%s",comand) != EOF){
    if(strcmp(comand,"rd") == 0) relaphComand(inpt,pathOut,city,shapes);
    else if(strcmp(comand,"xrd") == 0) xRelaphComand(inpt,pathOut,city,shapes);
    else if(strcmp(comand,"bo") == 0) bohmaComand(inpt,pathOut,city,shapes);
  }
}

void relaphComand(FILE *inpt, char **pathOut, City city, List *shapes){
  float x1, y1, x2, y2;

  fscanf(inpt,"%f %f %f %f",&x1,&y1,&x2,&y2);

  Rectangle aux = crtRect(x1,y1,x2-x1,y2-y1,-1,"lightCyan","black",'a');

  pushFront(shapes,aux);

  FILE *outTxt = CrtFileTxt(pathOut);
  if(outTxt != NULL){
    fprintf(outTxt,"%s %f %f %f %f\n","rd",x1,y1,x2,y2);
    int o = 0;
    if(getQtd(getHdrntTree(city)) == 0) fprintf(outTxt,"A cidade não possui hidrantes\n");
    else{
      relaphHydra(getHead(getHdrntTree(city)),x1,y1,x2,y2,&o,outTxt,"rd",city);
      if(o == 0) fprintf(outTxt,"Nenhum hidrante dentro da região\n");
    }
    fprintf(outTxt,"\n");
    fclose(outTxt);
  }
  else printf("Problemas com a criação do arquivo %s comando rd\n",*pathOut);
}

void xRelaphComand(FILE *inpt, char **pathOut, City city, List *shapes){
  float x1, y1, x2, y2;

  fscanf(inpt,"%f %f %f %f",&x1,&y1,&x2,&y2);

  Rectangle aux = crtRect(x1,y1,x2-x1,y2-y1,-1,"gray","orange",'b');
  pushFront(shapes,aux);

  FILE *outTxt = CrtFileTxt(pathOut);
  if(outTxt != NULL){
    fprintf(outTxt,"%s %f %f %f %f\n","xrd",x1,y1,x2,y2);
    int o = 0;
    if(getQtd(getHdrntTree(city)) == 0) fprintf(outTxt,"A cidade não possui hidrantes\n");
    else{
      relaphHydra(getHead(getHdrntTree(city)),x1,y1,x2,y2,&o,outTxt,"xrd",city);
      if(o == 0) fprintf(outTxt,"Nenhum hidrante dentro da região\n");
      if(getBlcksTree(city) != NULL) checkBlockXRelaph(getHead(getBlcksTree(city)),x1,y1,x2,y2,city);
      else fprintf(outTxt,"A cidade não possui quadras\n");
    }
    fprintf(outTxt,"\n");
    fclose(outTxt);
  }
  else printf("Problemas com a criação do arquivo %s comando xrd\n",*pathOut);
}

void bohmaComand(FILE *inpt, char **pathOut, City city, List *shapes){
  float x, y, r;
  fscanf(inpt,"%f %f %f",&x,&y,&r);

  Circle c1 = crtCrcl(x,y,r,"white","black",-1,'e');
  pushFront(shapes,c1);

  Circle c2 = crtCrcl(x,y,5,"black","black",-1,'f');
  pushFront(shapes,c2);

  FILE *outTxt = CrtFileTxt(pathOut);
  if(outTxt == NULL) printf("Problemas com a criação do arquivo %s comando bo\n",*pathOut);
  else{
    fprintf(outTxt,"%s %f %f %f\n","bo",x,y,r);
    int o = 0;
    if(getRdBsTree(city) != NULL) rdBsBohma(outTxt,x,y,r,getHead(getRdBsTree(city)),&o);
    else fprintf(outTxt,"Não existe rádio base nessa região\n");
    fprintf(outTxt,"\n");
    fclose(outTxt);
  }
}
