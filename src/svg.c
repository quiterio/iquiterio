#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cro.h"
#include "list.h"
#include "shape.h"
#include "rect.h"
#include "circle.h"
#include "equip.h"
#include "svg.h"
#include "block.h"
#include "city.h"
#include "file.h"

void pointsOfCircle(List *lRltPnt, List *lSHP, int i, float r, char *clr);
void pointsOfRect(List *lRltPnt, List *lSHP, int i, float r, char *clr);
void slctWrt(FILE *arq, List *obj);
void wrtOverLap(FILE *arq, Item list);
void wrtCrcl(FILE *arq, Item list);
void wrtPoint(FILE *arq, Item circl);
void wrtRct(FILE *arq, Item list);
void wrtBgn(FILE *arq);
void wrtXRelaphRect(FILE *arq, Rectangle rect);
void wrtRelaphRect(FILE *arq, Rectangle rect);
void wrtBohma(FILE *arq, Circle circle);

void crtSvg(char **sffx, char **path, City city, List *lSHP, int ctrl){
  char *out;
  FILE *outPS, *outpt;
  Tree pRootBlcksTree = getBlcksTree(city);
  Tree pRootHdrntTree = getHdrntTree(city);
  Tree pRootTrffcLghtTree = getTrffcLghtTree(city);
  Tree pRootRdBsTree = getRdBsTree(city);
  if (ctrl == 1) {
    outPS = CrtFileSVG(path,sffx);
    if(outPS == NULL) printf("Problemas com a criação do arquivo svg %s\n",*path);
    else{
      wrtBgn(outPS);
      slctWrt(outPS,lSHP);
      if(pRootBlcksTree != NULL) wrtBlck(outPS,getHead(pRootBlcksTree));
      if(pRootRdBsTree != NULL) wrtRdBs(outPS,getHead(pRootRdBsTree));
      if(pRootHdrntTree != NULL) wrtHydra(outPS,getHead(pRootHdrntTree));
      if(pRootTrffcLghtTree != NULL) wrtTrafficLght(outPS,getHead(pRootTrffcLghtTree));
      fprintf(outPS,"</svg>");
      fclose(outPS);
    }
  }
  else if(ctrl == 0){
    out = (char*)malloc((strlen(*path)+8)*sizeof(char));
    strcpy(out,*path);
    strcat(out,".svg");
    outpt = fopen(out,"w");
    if(outpt == NULL) printf("Problemas com a criação do arquivo svg %s\n",out);
    else{
      wrtBgn(outpt);
      slctWrt(outpt,lSHP);
      if(pRootBlcksTree != NULL) wrtBlck(outpt,getHead(pRootBlcksTree));
      if(pRootRdBsTree != NULL) wrtRdBs(outpt,getHead(pRootRdBsTree));
      if(pRootHdrntTree != NULL) wrtHydra(outpt,getHead(pRootHdrntTree));
      if(pRootTrffcLghtTree != NULL) wrtTrafficLght(outpt,getHead(pRootTrffcLghtTree));
      fprintf(outpt,"</svg>");
      fclose(outpt);
    }
  }

}

void crtPrtl(char **sffx, char *color, float r, char **path, List *lSHP)
{
  char type;
  int i, n;
  FILE *outPP;
  List lRltPnt;
  lRltPnt = createList();
  outPP = CrtFileSVG(path,sffx);
  if(outPP == NULL) printf("Problemas com a criação do arquivo svg %s\n",*path);
  else{
    n = getLen(*lSHP);
    for(i=0;i<n;i++)
    {
      type = getTypeShp(getItemN(*lSHP,i));
      if(type == 'r') pointsOfRect(&lRltPnt,lSHP,i,r,color);
      else if(type == 'c') pointsOfCircle(&lRltPnt,lSHP,i,r,color);
    }
    wrtBgn(outPP);
    slctWrt(outPP,&lRltPnt);
    fprintf(outPP,"</svg>");
    fclose(outPP);
  }
}

void pointsOfCircle(List *lRltPnt, List *lSHP, int i, float r, char *clr)
{
  float x, y;
  Circle aux;

  x = getXc(getItemN(*lSHP,i));
  y = getYc(getItemN(*lSHP,i));

  aux = crtCrcl(x,y,r,clr,"none",-1,'c');
  pushFront(lRltPnt,aux);
}


void pointsOfRect(List *lRltPnt, List *lSHP, int i, float r, char *clr)
{
  Circle aux;
  float x, y, w, h;

  x = getXr(getItemN(*lSHP,i));
  y = getYr(getItemN(*lSHP,i));
  w = getWr(getItemN(*lSHP,i));
  h = getHr(getItemN(*lSHP,i));

  aux = crtCrcl(x,y,r,clr,"none",-1,'c');
  pushFront(lRltPnt,aux);

  aux = crtCrcl(x+w,y,r,clr,"none",-1,'c');
  pushFront(lRltPnt,aux);

  aux = crtCrcl(x,y+h,r,clr,"none",-1,'c');
  pushFront(lRltPnt,aux);

  aux = crtCrcl(x+w,y+h,r,clr,"none",-1,'c');
  pushFront(lRltPnt,aux);
}

void slctWrt(FILE *arq, List *obj){
  int i=0;
  char type;

  for(i=0;i<getLen(*obj);i++){
    type = getTypeShp(getItemN(*obj,i));
    if(type == 'r') wrtRct(arq,getItemN(*obj,i));
    else if(type == 'c') wrtCrcl(arq,getItemN(*obj,i));
    else if(type == 'p') wrtPoint(arq,getItemN(*obj,i));
    else if(type == 's') wrtOverLap(arq,getItemN(*obj,i));
    else if(type == 'a') wrtRelaphRect(arq,getItemN(*obj,i));
    else if(type == 'b') wrtXRelaphRect(arq,getItemN(*obj,i));
    else if(type == 'e') wrtBohma(arq,getItemN(*obj,i));
    else if(type == 'f') wrtCrcl(arq,getItemN(*obj,i));
  }

}

void wrtOverLap(FILE *arq, Rectangle rect)
{
  float x, y, w, h;
  x = getXr(rect);
  y = getYr(rect);
  w = getWr(rect);
  h = getHr(rect);
  fprintf(arq,"\t<rect x=\"%.2f\" y=\"%.2f\" width=\"%.2f\" height=\"%.2f\" fill=\"none\"\n\t\tstyle=\"stroke:pink;stroke-width:3;stroke-dasharray:5,5\" />\n",x,y,w,h);
  fprintf(arq,"\t<text x=\"%.2f\" y=\"%.2f\" fill=\"red\">sobrepoe</text>\n",(x+2),(y+10));
}

void wrtCrcl(FILE *arq, Circle circl){
  float x, y, r;
  int id;
  char *fill, *stroke;
  x = getXc(circl);
  y = getYc(circl);
  r = getRc(circl);
  fill = getFllC(circl);
  stroke = getStrkC(circl);
  fprintf(arq,"\t<circle cx=\"%.2f\" cy=\"%.2f\" r=\"%.2f\" fill=\"%s\" stroke=\"%s\" />\n",x,y,r,fill,stroke);
}

void wrtPoint(FILE *arq, Circle circl){
  float x, y;
  char *fill, *stroke;
  int id;
  x = getXc(circl);
  y = getYc(circl);
  fill = getFllC(circl);
  stroke = getStrkC(circl);
  id = getIdShp(circl);
  fprintf(arq,"\t<circle cx=\"%.2f\" cy=\"%.2f\" r=\"20\" fill=\"%s\" stroke=\"%s\" />\n",x,y,fill,stroke);
  fprintf(arq,"\t<text x=\"%.2f\" y=\"%.2f\" fill=\"black\">P%d</text>\n",x-5,y+3,id);
}

void wrtRct(FILE *arq, Rectangle rect)
{
  float x, y, w, h;
  char *fill, *stroke;
  x = getXr(rect);
  y = getYr(rect);
  w = getWr(rect);
  h = getHr(rect);
  fill = getFllR(rect);
  stroke = getStrkR(rect);
  fprintf(arq,"\t<rect x=\"%.2f\" y=\"%.2f\" width=\"%.2f\" height=\"%.2f\" fill=\"%s\" stroke=\"%s\" />\n",x,y,w,h,fill,stroke);
}

void wrtBgn(FILE *arq)
{
  fprintf(arq,"<!-- Gustavo Mariotto e Igor Quitério -->\n");
  fprintf(arq,"<svg height=\"10000.00\" width=\"20000.00\">\n");
}

void wrtXRelaphRect(FILE *arq, Rectangle rect){
  float x, y, w, h;
  char *fill, *stroke;
  x = getXr(rect);
  y = getYr(rect);
  w = getWr(rect);
  h = getHr(rect);
  fill = getFllR(rect);
  stroke = getStrkR(rect);
  fprintf(arq,"\t<rect x=\"%.2f\" y=\"%.2f\" width=\"%.2f\" height=\"%.2f\" fill=\"%s\" stroke=\"%s\" stroke-width=\"3.0\" stroke-dasharray=\"4\" />\n",x,y,w,h,fill,stroke);
}

void wrtRelaphRect(FILE *arq, Rectangle rect){
  float x, y, w, h;
  char *fill, *stroke;
  x = getXr(rect);
  y = getYr(rect);
  w = getWr(rect);
  h = getHr(rect);
  fill = getFllR(rect);
  stroke = getStrkR(rect);
  fprintf(arq,"\t<rect x=\"%.2f\" y=\"%.2f\" width=\"%.2f\" height=\"%.2f\" fill=\"%s\" stroke=\"%s\" fill-opacity=\"0.8\" stroke-width=\"3.0\" />\n",x,y,w,h,fill,stroke);
}

void wrtBohma(FILE *arq, Circle circle){
  float x, y, r;
  int id;
  char *fill, *stroke;
  x = getXc(circle);
  y = getYc(circle);
  r = getRc(circle);
  fill = getFllC(circle);
  stroke = getStrkC(circle);
  fprintf(arq,"\t<circle cx=\"%.2f\" cy=\"%.2f\" r=\"%.2f\" fill=\"%s\" stroke=\"%s\" fill-opacity=\"0.8\" stroke-width=\"3.0\" />\n",x,y,r,fill,stroke);
}
