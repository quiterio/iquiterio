#ifndef HASH_H_
#define HASH_H_

#include "list.h"

typedef void* HashTable;

HashTable crtHashTable(int tamanho);
void insereHash (HashTable hashTb,Item anything, char* key);
void fndKeyList(HashTable head, char *key, List *lista);
Item fndKey(HashTable head, char* key);
int getLenHash(HashTable pHash);
void dltKeyHash(HashTable head, char *key);
void dltKeyListHash(HashTable head, char *key, Item data, int (*funComp) (Item,Item));

#endif
