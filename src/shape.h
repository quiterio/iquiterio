#ifndef SHAPE_H
#define SHAPE_H

#include "rect.h"
typedef void* Shape;
//Trata de forma genérica as formas geométrica circulo(Circle) e retângulo(Rect), representadas por um identificador
int getIdShp(Shape shap);
//Retorna o numero de id da forma geométrica(Shape)
char getTypeShp(Shape rect);
//Retorna o tipo da forma geométrica(Shape)

void overlap(int id1, int id2, List *shp);
int overlapRR(int id1, int id2, List shp);
int overlapCC(int id1, int id2, List shp);
int overlapCR(int cir, int rtc, List shp);
float distanceTo(float x1, float y1, float x2, float y2);
Rectangle fndPntsCR(int id1, int id2, List shp);
Rectangle fndPntsRR(int id1, int id2, List shp);
Rectangle fndPntsCC(int id1, int id2, List shp);
int inCC(float Bx, float By, float Br, Shape circl);
int inCR(float Cx, float Cy, float r, Shape rect);
int inRC(float Bx, float By, float Bw,float Bh, Shape circl);
int inRR(float Bx, float By, float Bw,float Bh, Shape rect);
Shape getItemIdShp(List *lSHP, int id);

#endif
