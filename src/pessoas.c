#include "city.h"
#include "equip.h"
#include "file.h"
#include "hash.h"
#include "list.h"
#include "pessoas.h"
#include "sort.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct _pessoa {
  char *cpf;
  char *nome;
  char *nascimento;
  char sexo;
} Pss;

typedef struct _moradores {
  char *cpf;
  char *cep;
  char *face;
  int num;
  char *telfixo;
} Mrdr;

typedef struct _celular { char *celular; } Cllr;

void habilitarCel(FILE *inpt, City city, char **comand, char **pathOut);
void desabilitarCel(FILE *inpt, City city, char **comand, char **pathOut);
void conectarCel(FILE *inpt, City city, char **comand, char **pathOut);
void rdBsCelulares(FILE *inpt, City city, char **comand, char **pathOut);
void celularRdBs(FILE *inpt, City city, char **comand, char **pathOut);
Pessoa criarPessoa(char *cpf, char *nome, char *nascimento, char sexo);
Morador criarMorador(char *cpf, char *cep, char *face, int num, char *telfixo);

void arqPessoas(FILE *arq, City city) {
  char cpf[20], nome[70], nascimento[20], sexo;
  while (fscanf(arq, "%s", cpf) != EOF) {
    fscanf(arq, "%s %s %c", nome, nascimento, &sexo);
    Pessoa aux = criarPessoa(cpf, nome, nascimento, sexo);
    if (getPessoasTree(city) == NULL) {
      Tree root = crtTree(&compTreePessoas, &dstrsPessoa);
      setPessoasTree(city, root);
    }
    addNode(getPessoasTree(city), aux);
    if (getCpfPessoaslHash(city) == NULL) {
      HashTable hash = crtHashTable(50000);
      setCpfPessoaslHash(city, hash);
    }
    insereHash(getCpfPessoaslHash(city), aux, cpf);
    if (getNomePessoasHash(city) == NULL) {
      HashTable hash = crtHashTable(50000);
      setNomePessoasHash(city, hash);
    }
    insereHash(getNomePessoasHash(city), aux, nome);
  }
}

void arqMoradoradores(FILE *arq, City city) {
  char cpf[20], cep[20], face[3], telfixo[20];
  int num;
  while (fscanf(arq, "%s", cpf) != EOF) {
    fscanf(arq, "%s %s %d %s", cep, face, &num, telfixo);
    Morador aux = criarMorador(cpf, cep, face, num, telfixo);
    if (getMoradoresTree(city) == NULL) {
      Tree root = crtTree(&compTreeMoradores, &dstrsMorador);
      setMoradoresTree(city, root);
    }
    addNode(getMoradoresTree(city), aux);
    if (getCepMoradoresHash(city) == NULL) {
      HashTable hash = crtHashTable(500);
      setCepMoradoresHash(city, hash);
    }
    insereHash(getCepMoradoresHash(city), aux, cep);
    if (getCpfMoradoresHash(city) == NULL) {
      HashTable hash = crtHashTable(500);
      setCpfMoradoresHash(city, hash);
    }
    insereHash(getCpfMoradoresHash(city), aux, cpf);
    if (getFoneMoradoresHash(city) == NULL) {
      HashTable hash = crtHashTable(500);
      setFoneMoradoresHash(city, hash);
    }
    insereHash(getFoneMoradoresHash(city), aux, telfixo);
  }
}

void arqCelular(FILE *inpt, City city, char **pathOut) {
  char *comand = malloc(5 * sizeof(char));
  while (fscanf(inpt, "%s", comand) != EOF) {
    if (strcmp(comand, "h") == 0)
      habilitarCel(inpt, city, &comand, pathOut);
    else if (strcmp(comand, "d") == 0)
      desabilitarCel(inpt, city, &comand, pathOut);
    else if (strcmp(comand, "p") == 0)
      conectarCel(inpt, city, &comand, pathOut);
    else if (strcmp(comand, "r") == 0)
      rdBsCelulares(inpt, city, &comand, pathOut);
    else if (strcmp(comand, "c") == 0)
      celularRdBs(inpt, city, &comand, pathOut);
  }
}

void habilitarCel(FILE *inpt, City city, char **comand, char **pathOut) {
  char cpf[15], cel[15];
  FILE *outTxt;

  fscanf(inpt, "%s %s", cpf, cel);

  outTxt = CrtFileTxt(pathOut);
  if (outTxt == NULL)
    printf("Problemas com a criação do arquivo %s comando h celular\n",
           *pathOut);
  else {
    fprintf(outTxt, "%s %s %s\n", *comand, cpf, cel);
    Pss *pessoa = (Pss *)fndKey(getCpfPessoaslHash(city), cpf);
    if (pessoa != NULL) {
      fprintf(outTxt, "%s %s %s\n", pessoa->cpf, pessoa->nome, cel);
      Cllr *aux = malloc(sizeof(Cllr));
      aux->celular = malloc((strlen(cel) + 1) * sizeof(char));
      strcpy(aux->celular, cel);
      if (getCelPessoaHash(city) == NULL) {
        HashTable c1 = crtHashTable(70000);
        setCelPessoaHash(city, c1);
      }
      insereHash(getCelPessoaHash(city), pessoa, cel);
      if (getCelularHash(city) == NULL) {
        HashTable c20 = crtHashTable(70000);
        setCelularHash(city, c20);
      }
      insereHash(getCelularHash(city), aux, cel);
    } else
      fprintf(outTxt, "Pessoa com cpf %s inexistente, celular não habilitado\n",
              cpf);
    fprintf(outTxt, "\n");
    fclose(outTxt);
  }
}

void desabilitarCel(FILE *inpt, City city, char **comand, char **pathOut) {
  char cel[15];
  FILE *outTxt;

  fscanf(inpt, "%s", cel);

  outTxt = CrtFileTxt(pathOut);
  if (outTxt == NULL)
    printf("Problemas com a criação do arquivo %s comando d celular\n",
           *pathOut);
  else {
    fprintf(outTxt, "%s %s\n", *comand, cel);

    Pss *pessoa = (Pss *)fndKey(getCelPessoaHash(city), cel);
    if (pessoa != NULL) {
      fprintf(outTxt, "%s %s\n", cel, pessoa->nome);
      dltKeyHash(getCelPessoaHash(city), cel);
      dltKeyHash(getCelularHash(city), cel);
      Equip eqp = fndKey(getCelRdioBsHash(city), cel);
      if (eqp != NULL) {
        Cllr *celu = malloc(sizeof(Cllr));
        celu->celular = malloc((strlen(cel) + 1) * sizeof(char));
        strcpy(celu->celular, cel);
        dltKeyListHash(getRdioBsCelularHash(city), getIdEqp(eqp), celu,
                       &compCelular);
        dltKeyHash(getCelRdioBsHash(city), cel);
        free(celu->celular);
        free(celu);
      }
    } else
      fprintf(outTxt, "O celular %s não foi habilitado\n", cel);
    fprintf(outTxt, "\n");
    fclose(outTxt);
  }
}

void conectarCel(FILE *inpt, City city, char **comand, char **pathOut) {
  float x, y;
  char cel[15];
  FILE *outTxt;

  fscanf(inpt, "%s %f %f", cel, &x, &y);

  outTxt = CrtFileTxt(pathOut);
  if (outTxt == NULL)
    printf("Problemas com a criação do arquivo %s comando p celular\n",
           *pathOut);
  else {
    fprintf(outTxt, "%s %s %f %f\n", *comand, cel, x, y);
    if (getRdBsTree(city) != NULL) {
      Equip eqp = fndKey(getCelRdioBsHash(city), cel);
      Cllr *cel1 = malloc(sizeof(Cllr));
      cel1->celular = malloc((strlen(cel) + 1) * sizeof(char));
      strcpy(cel1->celular, cel);
      dltKeyHash(getCelRdioBsHash(city), cel);
      dltKeyListHash(getRdioBsCelularHash(city), getIdEqp(eqp), cel1,
                     &compCelular);
      free(cel1->celular);
      free(cel1);
      List fake = createList();
      Item closer = RdioBsClsr(city, &fake, x, y, 1, '@');
      Item eqp1 = fndKey(getEquipsHash(city), getNameIRltvEqp(closer, 0));
      Cllr *aux = fndKey(getCelularHash(city), cel);
      if (aux == NULL)
        fprintf(outTxt, "Celular %s não habilitado\n", cel);
      else {
        fprintf(outTxt, "%s\n", getNameIRltvEqp(closer, 0));
        if (getRdioBsCelularHash(city) == NULL) {
          HashTable h10 = crtHashTable(1000);
          setRdioBsCelularHash(city, h10);
        }
        insereHash(getRdioBsCelularHash(city), aux, getNameIRltvEqp(closer, 0));
        if (getCelRdioBsHash(city) == NULL) {
          HashTable h20 = crtHashTable(2000);
          setCelRdioBsHash(city, h20);
        }
        insereHash(getCelRdioBsHash(city), eqp1, cel);
      }
    } else
      fprintf(outTxt, "A cidade não possui rádio-base\n");
    fprintf(outTxt, "\n");
    fclose(outTxt);
  }
}

void rdBsCelulares(FILE *inpt, City city, char **comand, char **pathOut) {
  char *rdioBs = malloc(20 * sizeof(char));
  FILE *outTxt;

  fscanf(inpt, "%s", rdioBs);
  outTxt = CrtFileTxt(pathOut);
  if (outTxt == NULL)
    printf("Problemas com a criação do arquivo %s comando r celular\n",
           *pathOut);
  else {
    fprintf(outTxt, "%s %s\n", *comand, rdioBs);
    if (getRdBsTree(city) == NULL)
      fprintf(outTxt, "A cidade não possui radio bases\n");
    else if (strcmp(rdioBs, "*") == 0)
      procuraCelRdioBs(city, outTxt, getHead(getRdBsTree(city)));
    else if (getRdioBsCelularHash(city) != NULL) {
      List listaCelulares = createList();
      fndKeyList(getRdioBsCelularHash(city), rdioBs, &listaCelulares);
      escreveCelNome(listaCelulares, outTxt, city);
    } else
      fprintf(outTxt, "Nenhum celular conectado a %s\n", rdioBs);
    fprintf(outTxt, "\n");
    fclose(outTxt);
  }
}

void celularRdBs(FILE *inpt, City city, char **comand, char **pathOut) {
  FILE *outTxt;
  char cel[15];

  fscanf(inpt, "%s", cel);

  outTxt = CrtFileTxt(pathOut);
  if (outTxt == NULL)
    printf("Problemas com a criação do arquivo %s comando c celular\n",
           *pathOut);
  else {
    fprintf(outTxt, "%s %s\n", *comand, cel);
    Equip aux = fndKey(getCelRdioBsHash(city), cel);
    if (aux != NULL)
      fprintf(outTxt, "%s %s\n", cel, getIdEqp(aux));
    else
      fprintf(outTxt, "Celular %s não foi conectado\n", cel);
    fprintf(outTxt, "\n");
    fclose(outTxt);
  }
}

Pessoa criarPessoa(char *cpf, char *nome, char *nascimento, char sexo) {
  Pss *aux = malloc(sizeof(Pss));
  aux->cpf = malloc((strlen(cpf) + 1) * sizeof(char));
  aux->nome = malloc((strlen(nome) + 1) * sizeof(char));
  aux->nascimento = malloc((strlen(nascimento) + 1) * sizeof(char));
  strcpy(aux->cpf, cpf);
  strcpy(aux->nome, nome);
  strcpy(aux->nascimento, nascimento);
  aux->sexo = sexo;
  return aux;
}

Morador criarMorador(char *cpf, char *cep, char *face, int num, char *telfixo) {
  Mrdr *aux = malloc(sizeof(Mrdr));
  aux->cpf = malloc(strlen(cpf) + 1 * sizeof(char));
  aux->cep = malloc(strlen(cep) + 1 * sizeof(char));
  aux->face = malloc((strlen(face) + 1) * sizeof(char));
  aux->num = num;
  aux->telfixo = malloc((strlen(telfixo) + 1) * sizeof(char));
  strcpy(aux->cpf, cpf);
  strcpy(aux->cep, cep);
  strcpy(aux->telfixo, telfixo);
  strcpy(aux->face, face);
  return aux;
}

int compTreePessoas(Pessoa pessoa1, Pessoa pessoa2) {
  Pss *aux1 = (Pss *)pessoa1;
  Pss *aux2 = (Pss *)pessoa2;
  if (strcmp(aux1->cpf, aux2->cpf) > 0)
    return 1;
  else if (strcmp(aux1->cpf, aux2->cpf) < 0)
    return -1;
  else if (strcmp(aux1->cpf, aux2->cpf) == 0)
    return 0;
}

int compTreeMoradores(Morador morador1, Morador morador2) {
  Mrdr *aux1 = (Mrdr *)morador1;
  Mrdr *aux2 = (Mrdr *)morador2;
  if (strcmp(aux1->cpf, aux2->cpf) > 0)
    return 1;
  else if (strcmp(aux1->cpf, aux2->cpf) < 0)
    return -1;
  else if (strcmp(aux1->cpf, aux2->cpf) == 0)
    return 0;
}

void escreveDadosMorador(FILE *outTxt, List lista, City city) {
  for (int i = 0; i < getLen(lista); i++) {
    Mrdr *morador = getItemN(lista, i);
    Pss *pessoa = fndKey(getCpfPessoaslHash(city), morador->cpf);
    if (pessoa != NULL) {
      fprintf(outTxt, "%s %s %s numero:%d %s\n", pessoa->nome, morador->cep,
              morador->face, morador->num, morador->telfixo);
    } else {
      fprintf(outTxt,
              "cpf do morador não consta como cpf de uma pessoa cadastrada");
    }
  }
}

void prcrMrds(FILE *outTxt, char *cep, City city) {
  List listaMorCep = createList();
  fndKeyList(getCepMoradoresHash(city), cep, &listaMorCep);
  if (getLen(listaMorCep) == 0)
    fprintf(outTxt, "Não existe nenhum morador no cep infomado\n");
  escreveDadosMorador(outTxt, listaMorCep, city);
}

char *getNomePessoa(Pessoa pPessoa) {
  Pss *aux = (Pss *)pPessoa;
  if (aux != NULL)
    return aux->nome;
  return "erro";
}

char *getCpfPessoa(Pessoa pPessoa) {
  Pss *aux = (Pss *)pPessoa;
  if (aux != NULL)
    return aux->cpf;
  return "erro";
}

char *getCepMorador(Morador pMorador) {
  Mrdr *aux = (Mrdr *)pMorador;
  if (aux != NULL)
    return aux->cep;
  return "erro";
}

char *getCpfMorador(Morador pMorador) {
  Mrdr *aux = (Mrdr *)pMorador;
  if (aux != NULL)
    return aux->cpf;
  return "erro";
}

char *getFaceMorador(Morador pMorador) {
  Mrdr *aux = (Mrdr *)pMorador;
  if (aux != NULL)
    return aux->face;
  return "erro";
}

int getNumMorador(Morador pMorador) {
  Mrdr *aux = (Mrdr *)pMorador;
  if (aux != NULL)
    return aux->num;
  return 0;
}

void escreveCelNome(List listaCelulares, FILE *outTxt, City city) {
  for (int i = 0; i < getLen(listaCelulares); i++) {
    Cllr *cellr = (Cllr *)getItemN(listaCelulares, i);
    Pss *pessoa = (Pss *)fndKey(getCelPessoaHash(city), cellr->celular);
    fprintf(outTxt, "%s %s\n", cellr->celular, pessoa->nome);
  }
}

int compCelular(Celular cel1, Celular cel2) {
  Cllr *aux1 = (Cllr *)cel1;
  Cllr *aux2 = (Cllr *)cel2;
  if (strcmp(aux1->celular, aux2->celular) == 0)
    return 0;
  else if (strcmp(aux1->celular, aux2->celular) < 0)
    return -1;
  else
    return 1;
}

void dstrsMorador(Morador morador) {
  Mrdr *aux = (Mrdr *)morador;
  if (aux != NULL) {
    free(aux->cpf);
    free(aux->cep);
    free(aux->face);
    free(aux->telfixo);
    free(aux);
  }
}

void dstrsPessoa(Pessoa pessoa) {
  Pss *aux = (Pss *)pessoa;
  if (aux != NULL) {
    free(aux->cpf);
    free(aux->nome);
    free(aux->nascimento);
    free(aux);
  }
}
