#ifndef PESSOA_H_
#define PESSOA_H_
#include <stdio.h>
#include "city.h"
#include "list.h"

typedef void* Pessoa;
typedef void* Morador;
typedef void* Celular;

void arqPessoas(FILE *arq, City city);
int compTreePessoas(Pessoa pessoa1, Pessoa pessoa2);
void arqMoradoradores(FILE *arq,City city);
void arqCelular(FILE *inpt, City city, char **pathOut);
int compTreeMoradores(Morador morador1, Morador morador2);
void prcrMrds(FILE *outTxt, char *cep, City city);
void escreveDadosMorador(FILE *outTxt, List lista, City city);
char* getNomePessoa(Pessoa pPessoa);
char *getCpfPessoa(Pessoa pPessoa);
char *getCepMorador(Morador pMorador);
char *getCpfMorador(Morador pMorador);
char *getFaceMorador(Morador pMorador);
int getNumMorador(Morador pMorador);
void escreveCelNome(List listaCelulares, FILE *outTxt, City city);
int compCelular(Celular cel1, Celular cel2);
void dstrsMorador(Morador morador);
void dstrsPessoa(Pessoa pessoa);

#endif
