#include "arvore.h"
#include "city.h"
#include "comercio.h"
#include "cro.h"
#include "dcmons.h"
#include "equip.h"
#include "file.h"
#include "list.h"
#include "pessoas.h"
#include "shape.h"
#include "svg.h"
#include "viario.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void main(int argc, char *argv[]) {
  FILE *inpt, *outpt;
  char *pathOut, *out, *pathInpt;
  City city;
  List shapes = createList();
  city = crtCity();
  pathInpt = findPathInpt(argc, argv);
  pathOut = findPathOut(argc, argv);

  inpt = findInpt(argc, argv, "-t");
  if (inpt != NULL) {
    arqTpEst(inpt, city);
    fclose(inpt);
  }

  inpt = findInpt(argc, argv, "-e");
  if (inpt != NULL) {
    arqEstCom(inpt, city);
    fclose(inpt);
  }

  inpt = findInpt(argc, argv, "-p");
  if (inpt != NULL) {
    arqPessoas(inpt, city);
    fclose(inpt);
  }

  inpt = findInpt(argc, argv, "-m");
  if (inpt != NULL) {
    arqMoradoradores(inpt, city);
    fclose(inpt);
  }

  inpt = findInpt(argc, argv, "-f");
  if (inpt != NULL) {
    arqCro(inpt, &pathOut, &pathInpt, city, &shapes);
    fclose(inpt);
  }

  inpt = findInpt(argc, argv, "-c");
  if (inpt != NULL) {
    arqCelular(inpt, city, &pathOut);
    fclose(inpt);
  }

  inpt = findInpt(argc, argv, "-g");
  if (inpt != NULL) {
    armazenaViario(inpt, city);
    fclose(inpt);
  }

  inpt = findInpt(argc, argv, "-q");
  if (inpt != NULL) {
    ConsultasViario(inpt, &pathOut, city);
    fclose(inpt);
  }

  inpt = findInpt(argc, argv, "-d");
  if (inpt != NULL) {
    dcMons(inpt, &pathOut, city, &shapes);
    fclose(inpt);
  }

  // printTree(getBlcksTree(city),&getIdBlock,pathOut);
  crtSvg(&out, &pathOut, city, &shapes, 0);
}
