#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "list.h"
#include "cro.h"
#include "polygon.h"
#include "equip.h"
#include "city.h"
#include "arvore.h"
#include "hash.h"
#include "file.h"
#include "block.h"
#include "shape.h"
#include "pessoas.h"

typedef struct equip{
  char type;
  char id[50];
  float x, y;
  int pos;
  char clr[20];
  int ligado;
}Eqp;

void chckEqp(char *id, HashTable equips, List *lPlgn, FILE *outTxt){
  int o1, o2, i, j;
  float xP1, yP1, xP2, yP2;
  int v, u;
  Eqp *aux = fndKey(equips,id);
  if(aux != NULL){

    v = pointInPolygon(lPlgn,aux->x,aux->y);
    u = 0;
    if(v == 1){
    for(i=0;i<getLen(lPlgn)-1;i++){
        j = i + 1;
        xP1 = getXPlgn(getItemN(lPlgn,i));
        xP2 = getXPlgn(getItemN(lPlgn,j));
        yP1 = getYPlgn(getItemN(lPlgn,i));
        yP2 = getYPlgn(getItemN(lPlgn,j));
        o1 = orientation(xP1,yP1,xP2,yP2,aux->x,aux->y);
        if(o1 == 0 && (onSegment(xP1,yP1,aux->x,aux->y,xP2,yP2) == 1)) u = 1;
      }
    }
    if(u == 0 && v == 1) fprintf(outTxt,"Equipamento dentro\n");
    else fprintf(outTxt,"Equipamento fora\n");
    fprintf(outTxt,"\n");
  }
  else fprintf(outTxt,"Equipamento com Id inexistente\n");
}

void* crtEqp(float pX, float pY, char *pId, char type){
  Eqp *aux = malloc(sizeof(Eqp));
  aux->x = pX;
  aux->y = pY;
  strcpy(aux->id,pId);
  aux->type = type;
  aux->pos = -1;
  if(type == 'd') aux->ligado = 1;
  else aux->ligado = 0;
  return (Equip) aux;
}

void wrtTrafficLght(FILE *arq, Node pNode){
  if(pNode != NULL){
    Eqp *aux = (Eqp*)infoNo(pNode);
    float x = aux->x;
    float y = aux->y;
    float r = 10;
    fprintf(arq,"\t<polygon points=\"%.2f,%.2f %.2f,%.2f %.2f,%.2f\" style=\"fill:lawngreen;stroke:lawngreen;stroke-width:1\" />\n",x,(y-3*r/2),(x-(3.4*r/3)),(y+r/3),(x+3.4*r/3),(y+r/3));
    fprintf(arq,"<text text-anchor=\"middle\" x=\"%.2f\" y=\"%.2f\" fill=\"white\">S</text>\n",x,y);
    wrtTrafficLght(arq,getLeft(pNode));
    wrtTrafficLght(arq,getRight(pNode));
  }
}

void wrtHydra(FILE *arq, Node pNode){
  if(pNode != NULL){
    Eqp *aux = (Eqp*)infoNo(pNode);
    float x = aux->x;
    float y =aux->y;
    char stroke[20];
    int pos = aux->pos;
    if(pos >= 0){
      strcpy(stroke,aux->clr);
      fprintf(arq,"\t<circle cx=\"%.2f\" cy=\"%.2f\" r=\"20\" fill=\"none\" stroke=\"%s\" />\n",x,y,stroke);
      if(pos > 0) fprintf(arq,"\t<text text-anchor=\"middle\" x=\"%.2f\" y=\"%.2f\" fill=\"black\" stroke-width=\"1\">%d</text>\n",x+6,y-13,pos);
    }
    fprintf(arq,"\t<circle cx=\"%.2f\" cy=\"%.2f\" r=\"10\" fill=\"red\" />\n",x,(y-3));
    fprintf(arq,"\t<text text-anchor=\"middle\" x=\"%.2f\" y=\"%.2f\" fill=\"white\">H</text>\n",x,y);
    wrtHydra(arq,getLeft(pNode));
    wrtHydra(arq,getRight(pNode));
  }
}

void wrtRdBs(FILE *arq, Node pNode){
  if(pNode != NULL){
    Eqp *aux = (Eqp*)infoNo(pNode);
    float x = aux->x;
    float y = aux->y;
    float r = 10;
    int pos = aux->pos;
    char stroke[20];
    if(pos >= 0){
      strcpy(stroke,aux->clr);
      fprintf(arq,"\t<circle cx=\"%.2f\" cy=\"%.2f\" r=\"20\" fill=\"none\" stroke=\"%s\" />\n",x,y,stroke);
      if(pos > 0) fprintf(arq,"\t<text x=\"%.2f\" y=\"%.2f\" fill=\"black\">%d</text>\n",x+6,y-13,pos);
    }
    fprintf(arq,"\t<polygon points=\"%.2f,%.2f %.2f,%.2f %.2f,%.2f %.2f,%.2f %.2f,%.2f %.2f,%.2f %.2f,%.2f %.2f,%.2f\"\n",(x-r/2),(y+r),(x+r/2),(y+r),(x+r),(y+r/2),(x+r),(y-r/2),(x+r/2),(y-r),(x-r/2),(y-r),(x-r),(y-r/2),(x-r),(y+r/2));
    fprintf(arq,"style=\"fill:mediumslateblue;stroke:gold;stroke-width:1\" />\n");
    fprintf(arq,"\t<text text-anchor=\"middle\" x=\"%.2f\" y=\"%.2f\" fill=\"white\">T</text>\n",x,y+r/3);
    wrtRdBs(arq,getLeft(pNode));
    wrtRdBs(arq,getRight(pNode));
  }
}

float getXEqp(Equip elemnt){
  Eqp *aux = (Eqp*) elemnt;
  if(aux != NULL) return aux->x;
  return -1;
}

float getYEqp(Equip elemnt){
  Eqp *aux = (Eqp*) elemnt;
  if(aux != NULL) return aux->y;
  return -1;
}

char getTypeEqp(Equip elemnt){
  Eqp *aux = (Eqp*) elemnt;
  if(aux != NULL) return aux->type;
  return 'z';
}

char* getIdEqp(Equip elemnt){
  Eqp *aux = (Eqp*) elemnt;
  if(aux != NULL) return aux->id;
  return "erro";
}

Equip getItemIdEqp(List *lEqp, char *id){
  int i;
  char searchId[50];
  for(i=0;i<getLen(*lEqp);i++){
    strcpy(searchId,getIdEqp(getItemN(*lEqp,i)));
    if(strcmp(searchId,id) == 0) return getItemN(*lEqp,i);
  }
  return NULL;
}

void setPosEqp(Equip elemnt, int p){
  Eqp *aux = (Eqp*) elemnt;
  if(aux != NULL) aux->pos = p;
}

int getPosEqp(Equip elemnt){
  Eqp *aux = (Eqp*) elemnt;
  if(aux != NULL) return aux->pos;
}

void setClrEqp(Equip elemnt, char *c){
  Eqp *aux = (Eqp*) elemnt;
  if(aux != NULL) strcpy(aux->clr,c);
}

char* getClrEqp(Equip elemnt){
  Eqp *aux = (Eqp*) elemnt;
  if(aux != NULL) return aux->clr;
  return "erro";
}

int getLigadoEqp(Equip elemnt){
  Eqp *aux = (Eqp*) elemnt;
  if(aux != NULL) return aux->ligado;
  return -1;
}

int compTreeEquip(Equip eqp1, Equip eqp2){
  Eqp *aux1 = (Eqp*) eqp1;
  Eqp *aux2 = (Eqp*) eqp2;
  if(aux1->x == aux2->x && aux1->y == aux2->y) return 0;
  if(aux1->x > aux2->x) return 1;
  else if(aux1->x < aux2->x) return -1;
  else if(aux1->y > aux2->y) return 2;
  else if(aux1->y < aux2->y) return -2;
}

void relaphHydra(Node pNode, float x1, float y1, float x2, float y2, int *o, FILE *outTxt, char *comand, City city){
  if(pNode != NULL){
    Eqp *aux = (Eqp*) infoNo(pNode);
    if(aux->x < x1) relaphHydra(getRight(pNode),x1,y1,x2,y2,o,outTxt,comand,city);
    else if(aux->x > x2) relaphHydra(getLeft(pNode),x1,y1,x2,y2,o,outTxt,comand,city);
    else{
      if(aux->y >= y1 && aux->y <= y2){
        *o = 1;
        fprintf(outTxt,"%s %.f %f\n",aux->id,aux->x,aux->y);
      }
      relaphHydra(getRight(pNode),x1,y1,x2,y2,o,outTxt,comand,city);
      relaphHydra(getLeft(pNode),x1,y1,x2,y2,o,outTxt,comand,city);
    }
  }
}

void equipXRelaphHydra(Node pNode, City city, float x1, float y1, float x2, float y2, int *o){
  if(pNode != NULL){
    Eqp *aux = (Eqp*) infoNo(pNode);
    if(aux->x < x1) equipXRelaphHydra(getRight(pNode),city,x1,y1,x2,y2,o);
    else if(aux->x > x2) equipXRelaphHydra(getLeft(pNode),city,x1,y1,x2,y2,o);
    else{
        if(aux->y >= y1 && aux->y <= y2){
        *o = 1;
        dltKeyHash(getEquipsHash(city),aux->id);
        rmvData(aux,getHdrntTree(city));
      }
      equipXRelaphHydra(getRight(pNode),city,x1,y1,x2,y2,o);
      equipXRelaphHydra(getLeft(pNode),city,x1,y1,x2,y2,o);
    }
  }
}

void equipXRelaphRdBs(Node pNode, City city, float x1, float y1, float x2, float y2){
  if(pNode != NULL){
    Eqp *aux = (Eqp*) infoNo(pNode);
    if(aux->x < x1) equipXRelaphRdBs(getRight(pNode),city,x1,y1,x2,y2);
    else if(aux->x > x2) equipXRelaphRdBs(getLeft(pNode),city,x1,y1,x2,y2);
    else{
      if(aux->y >= y1 && aux->y <= y2){
        dltKeyHash(getEquipsHash(city),aux->id);
        rmvData(aux,getRdBsTree(city));
      }
      equipXRelaphRdBs(getRight(pNode),city,x1,y1,x2,y2);
      equipXRelaphRdBs(getLeft(pNode),city,x1,y1,x2,y2);
    }
  }
}

void equipXRelaphTrffcLght(Node pNode, City city, float x1, float y1, float x2, float y2){
  if(pNode != NULL){
    Eqp *aux = (Eqp*) infoNo(pNode);
    if(aux->x < x1) equipXRelaphTrffcLght(getRight(pNode),city,x1,y1,x2,y2);
    else if(aux->x > x2) equipXRelaphTrffcLght(getLeft(pNode),city,x1,y1,x2,y2);
    else{
      if(aux->y >= y1 && aux->y <= y2){
        dltKeyHash(getEquipsHash(city),aux->id);
        rmvData(aux,getTrffcLghtTree(city));
      }
      equipXRelaphTrffcLght(getRight(pNode),city,x1,y1,x2,y2);
      equipXRelaphTrffcLght(getLeft(pNode),city,x1,y1,x2,y2);
    }
  }
}

void rdBsBohma(FILE *outTxt, float x, float y, float r, Node pNode, int *o){
  if(pNode != NULL){
    Eqp *aux = (Eqp*) infoNo(pNode);
    if(aux->x < x-r) rdBsBohma(outTxt,x,y,r,getRight(pNode),o);
    else if(aux->x > x+r) rdBsBohma(outTxt,x,y,r,getLeft(pNode),o);
    else{
      if(distanceTo(x,y,aux->x,aux->y) <= r){
        *o = 1;
        fprintf(outTxt,"%s %f %f\n",aux->id,aux->x,aux->y);
        aux->ligado = 0;
      }
      rdBsBohma(outTxt,x,y,r,getLeft(pNode),o);
      rdBsBohma(outTxt,x,y,r,getRight(pNode),o);
    }
  }
}

void procuraCelRdioBs(City city, FILE *outTxt, Node pNode){
  if(pNode != NULL){
    Eqp *aux = (Eqp*) infoNo(pNode);
    List listaCelulares = createList();
    fndKeyList(getRdioBsCelularHash(city),aux->id,&listaCelulares);
    if(getLen(listaCelulares) > 0){
      fprintf(outTxt,"Radio Base %s Celulares e Nomes: \n",aux->id);
      escreveCelNome(listaCelulares,outTxt,city);
    }
    else fprintf(outTxt,"Nenhum celular conectado a %s\n",aux->id);
    procuraCelRdioBs(city,outTxt,getLeft(pNode));
    procuraCelRdioBs(city,outTxt,getRight(pNode));
  }
}

void dstrsEquip(Equip eqp){
  Eqp *aux = (Eqp*) eqp;
  if(aux != NULL){
    free(aux);
  }
}
