#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"
#include "shape.h"
#include "rect.h"
#include "circle.h"


typedef struct Rect{
  char type;
  int id;
  float x, y, w, h;
  char fill[20], stroke[20];
}Rct;

Rectangle crtRect(float pX, float pY, float pW, float pH, int pId, char clrFill[20],char clrStroke[20], char pType){
  Rct *aux = (Rct*)malloc(sizeof(Rct));
  aux->x = pX;
  aux->y = pY;
  aux->w = pW;
  aux->h = pH;
  aux->id = pId;
  strcpy(aux->fill,clrFill);
  strcpy(aux->stroke,clrStroke);
  aux->type = pType;
  return (Rectangle) aux;
}


void chngInptClrRect(char *newFll, char *newStrk, char *fill, char *stroke)
{
  if(strcmp(newFll,"-") == 0) strcpy(newFll,"none");
  if(strcmp(newStrk,"-") == 0) strcpy(newStrk,"none");

  strcpy(fill,newFll);
  strcpy(stroke,newStrk);
}


void cmpRect(float x, float y, float w, float h, char *c1, char *c2, List *shp){
  int n, i , itsIn;
  char type;

  n = 0;
  for(i=0;i<getLen(*shp);i++){
    slctShpCmpR(x,y,w,h,c1,c2,shp,i,&n);
  }
  printf("comparacoes xr = %d\n",n);
}

void slctShpCmpR(float x, float y,float w,float h, char *c1, char *c2, List *shp, int i, int *n){
  char color[20], type;
  int itsIn;
  itsIn = 0;
  type = getTypeShp(getItemN(*shp,i));
  if(type == 'c'){
    strcpy(color,getFllC(getItemN(*shp,i)));
    if (strcmp(c1,color) == 0 || strcmp(c1,"*") == 0){
      itsIn = inRC(x,y,w,h, getItemN(*shp,i));
      if (itsIn == 1) chngClrC(getItemN(*shp,i), c2);
    }
    *n += 1;
  }else if(type == 'r'){
    strcpy(color,getFllR(getItemN(*shp,i)));
    if (strcmp(c1,color) == 0 || strcmp(c1,"*") == 0){
      itsIn = inRR(x,y,w,h, getItemN(*shp,i));
      if (itsIn == 1) chngClrR(getItemN(*shp,i), c2);
    }
    *n += 1;
  }
}

void chngClrR(Rectangle rect, char *newcolor){
  Rct *aux = (Rct*) rect;
  strcpy(aux->fill,newcolor);
}

float getXr(Rectangle rect){
  Rct *aux = (Rct*) rect;
  return aux->x;
}

float getYr(Rectangle rect){
  Rct *aux = (Rct*) rect;
  return aux->y;
}

float getWr(Rectangle rect){
  Rct *aux = (Rct*) rect;
  return aux->w;
}

float getHr(Rectangle rect){
  Rct *aux = (Rct*) rect;
  return aux->h;
}

char* getFllR(Rectangle rect){
  Rct *aux = (Rct*) rect;
  return aux->fill;
}

char* getStrkR(Rectangle rect){
  Rct *aux = (Rct*) rect;
  return aux->stroke;
}
