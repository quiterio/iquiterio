#include "arvore.h"
#include "city.h"
#include "hash.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct ct {
  Tree blcksTree;
  Tree hdrntTree;
  Tree trffcLghtTree;
  Tree rdBsTree;
  Tree estComercialTree;
  Tree tipoComercioTree;
  Tree pessoasTree;
  Tree moradoresTree;
  HashTable ruaHash;
  HashTable cruzamentoHash;
  HashTable equipsHash;
  HashTable blocksHash;
  HashTable cepComercialHash;
  HashTable cpfPessoasHash;
  HashTable NomePessoasHash;
  HashTable cepMoradoresHash;
  HashTable cpfMoradoresHash;
  HashTable FoneMoradoresHash;
  HashTable codTpEstComHash;
  HashTable celPessoaHash;
  HashTable rdioBsCelularHash;
  HashTable celRdioBsHash;
  HashTable celularHash;
} cty;

City crtCity() {
  cty *aux = malloc(sizeof(cty));
  aux->blcksTree = NULL;
  aux->hdrntTree = NULL;
  aux->trffcLghtTree = NULL;
  aux->rdBsTree = NULL;
  aux->estComercialTree = NULL;
  aux->tipoComercioTree = NULL;
  aux->pessoasTree = NULL;
  aux->moradoresTree = NULL;
  aux->ruaHash = NULL;
  aux->cruzamentoHash = NULL;
  aux->equipsHash = NULL;
  aux->blocksHash = NULL;
  aux->cepComercialHash = NULL;
  aux->cpfPessoasHash = NULL;
  aux->NomePessoasHash = NULL;
  aux->cepMoradoresHash = NULL;
  aux->cpfMoradoresHash = NULL;
  aux->FoneMoradoresHash = NULL;
  aux->codTpEstComHash = NULL;
  aux->celPessoaHash = NULL;
  aux->rdioBsCelularHash = NULL;
  aux->celRdioBsHash = NULL;
  aux->celularHash = NULL;
  return aux;
}

/* set Hash  */
void setNomePessoasHash(City city, HashTable nomes) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->NomePessoasHash = nomes;
}

void setRuaHash(City city, HashTable arestas) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->ruaHash = arestas;
}

void setCruzamentoHash(City city, HashTable cruzamentos) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->cruzamentoHash = cruzamentos;
}

void setBlocksHash(City city, HashTable blocks) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->blocksHash = blocks;
}

void setEquipsHash(City city, HashTable equips) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->equipsHash = equips;
}

void setCepComercialHash(City city, HashTable cepCom) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->cepComercialHash = cepCom;
}

void setCpfPessoaslHash(City city, HashTable cpfPess) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->cpfPessoasHash = cpfPess;
}

void setCepMoradoresHash(City city, HashTable cepMor) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->cepMoradoresHash = cepMor;
}

void setCpfMoradoresHash(City city, HashTable cpfMor) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->cpfMoradoresHash = cpfMor;
}

void setFoneMoradoresHash(City city, HashTable foneMor) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->FoneMoradoresHash = foneMor;
}

void setCodTpEstComHash(City city, HashTable pCodTpEstComHash) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->codTpEstComHash = pCodTpEstComHash;
}

void setCelPessoaHash(City city, HashTable pCelPessoaHash) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->celPessoaHash = pCelPessoaHash;
}

void setRdioBsCelularHash(City city, HashTable pRadioBsCelularHash) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->rdioBsCelularHash = pRadioBsCelularHash;
}

void setCelRdioBsHash(City city, HashTable pCelRdioBsHash) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->celRdioBsHash = pCelRdioBsHash;
}

void setCelularHash(City city, HashTable pCelularHash) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->celularHash = pCelularHash;
}

/* set Tree */
void setBlcksTree(City city, Tree root) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->blcksTree = root;
}

void setHdrntTree(City city, Tree root) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->hdrntTree = root;
}

void setTrffcLghtTree(City city, Tree root) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->trffcLghtTree = root;
}

void setRdBsTree(City city, Tree root) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->rdBsTree = root;
}

void setEstComercialTree(City city, Tree root) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->estComercialTree = root;
}

void setTipoComercioTree(City city, Tree root) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->tipoComercioTree = root;
}

void setPessoasTree(City city, Tree root) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->pessoasTree = root;
}

void setMoradoresTree(City city, Tree root) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    aux->moradoresTree = root;
}

/* get Hash */
HashTable getRuaHash(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->ruaHash;
  return aux;
}

HashTable getCruzamentoHash(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->cruzamentoHash;
  return aux;
}

HashTable getBlocksHash(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->blocksHash;
  return NULL;
}

HashTable getEquipsHash(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->equipsHash;
  return NULL;
}

HashTable getCepComercialHash(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->cepComercialHash;
  return NULL;
}

HashTable getCpfPessoaslHash(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->cpfPessoasHash;
  return NULL;
}

HashTable getNomePessoasHash(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->NomePessoasHash;
  return NULL;
}

HashTable getCepMoradoresHash(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->cepMoradoresHash;
  return NULL;
}

HashTable getCpfMoradoresHash(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->cpfMoradoresHash;
  return NULL;
}

HashTable getFoneMoradoresHash(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->FoneMoradoresHash;
  return NULL;
}

HashTable getCodTpEstComHash(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->codTpEstComHash;
  return NULL;
}

HashTable getCelPessoaHash(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->celPessoaHash;
  return NULL;
}

HashTable getRdioBsCelularHash(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->rdioBsCelularHash;
  return NULL;
}

HashTable getCelRdioBsHash(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->celRdioBsHash;
  return NULL;
}

HashTable getCelularHash(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->celularHash;
  return NULL;
}

/* get Tree */
Tree getBlcksTree(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->blcksTree;
  return NULL;
}

Tree getHdrntTree(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->hdrntTree;
  return NULL;
}

Tree getTrffcLghtTree(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->trffcLghtTree;
  return NULL;
}

Tree getRdBsTree(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->rdBsTree;
  return NULL;
}

Tree getEstComercialTree(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->estComercialTree;
  return NULL;
}

Tree getTipoComercioTree(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->tipoComercioTree;
  return NULL;
}

Tree getPessoasTree(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->pessoasTree;
  return NULL;
}

Tree getMoradoresTree(City city) {
  cty *aux = (cty *)city;
  if (aux != NULL)
    return aux->moradoresTree;
  return NULL;
}
