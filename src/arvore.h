#ifndef ARV_H_
#define ARV_H_
#include "list.h"

typedef void* Data;
typedef void* Node;
typedef void* Tree;

Data infoNo(Node pNode);
Tree crtTree(int (*comp) (Data,Data), void (*dest) (Data));
void addNode(Tree pTree, Data pData);
void rmvNode(Tree pTree, Node pNode);
Node getRight(Node pNode);
Node getLeft(Node pNode);
int getQtd(Tree pTree);
Node getHead(Tree pTree);
void getDistancesTree(Tree pNode, float pX, float pY, List *pntsOn, float (distance) (float,float,float,float));
Node getNode(Tree pTree, Data data);
void rmvData(Data pData, Tree pTree);

#endif
