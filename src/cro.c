#include "arvore.h"
#include "block.h"
#include "circle.h"
#include "city.h"
#include "comercio.h"
#include "cro.h"
#include "equip.h"
#include "file.h"
#include "hash.h"
#include "list.h"
#include "pessoas.h"
#include "polygon.h"
#include "rect.h"
#include "shape.h"
#include "sort.h"
#include "svg.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void arqCro(FILE *inpt, char **pathOut, char **pathInpt, City city,
            List *shapes) {
  int n;
  char comand[20];
  char FllR[20] = "cyan", StrkR[20] = "none";
  char FllC[20] = "gray", StrkC[20] = "none";
  fscanf(inpt, "%s", comand);
  n = 0;
  while (strcmp(comand, "#") != 0) {
    if (strcmp(comand, "nt?") == 0 || strcmp(comand, "nh?") == 0 ||
        strcmp(comand, "ft?") == 0 || strcmp(comand, "fh?") == 0 ||
        strcmp(comand, "nrt?") == 0 || strcmp(comand, "nrh?") == 0)
      n++;
    slctCmd(inpt, pathOut, pathInpt, comand, FllC, StrkC, FllR, StrkR, city,
            shapes, n);
    fscanf(inpt, "%s", comand);
  }
}

void slctCmd(FILE *inpt, char **pathOut, char **pathInpt, char *comand,
             char *FllC, char *StrkC, char *FllR, char *StrkR, City city,
             List *shapes, int n) {
  float x, y, w, h, r;
  float x1, x2, y1, y2;
  int id, id1, id2, k, k1, k2;
  char type1, newFll[20], newStrk[20], c1[20], c2[20], *sffx, cep[50], *pol, o,
      cod[10];
  if (strcmp(comand, "r") == 0) {
    Rectangle *aux;
    fscanf(inpt, "%f %f %f %f %d", &x, &y, &w, &h, &id);
    aux = crtRect(x, y, w, h, id, FllR, StrkR, 'r');
    pushFront(shapes, aux);
  }
  if (strcmp(comand, "c") == 0) {
    Circle *aux;
    fscanf(inpt, "%f %f %f %d", &x, &y, &r, &id);
    aux = crtCrcl(x, y, r, FllC, StrkC, id, 'c');
    pushFront(shapes, aux);
  }
  if (strcmp(comand, "o") == 0) {
    fscanf(inpt, "%d %d", &id1, &id2);
    overlap(id1, id2, shapes);
  }
  if (strcmp(comand, "cc") == 0) {
    fscanf(inpt, "%s %s", newFll, newStrk);
    chngInptClrCirc(newFll, newStrk, FllC, StrkC);
  }
  if (strcmp(comand, "cr") == 0) {
    fscanf(inpt, "%s %s", newFll, newStrk);
    chngInptClrRect(newFll, newStrk, FllR, StrkR);
  }
  if (strcmp(comand, "xc") == 0) {
    fscanf(inpt, "%f %f %f %s %s", &x, &y, &r, c1, c2);
    cmpCirc(x, y, r, c1, c2, shapes);
  }
  if (strcmp(comand, "xr") == 0) {
    fscanf(inpt, "%f %f %f %f %s %s", &x, &y, &w, &h, c1, c2);
    cmpRect(x, y, w, h, c1, c2, shapes);
  }
  if (strcmp(comand, "ps") == 0) {
    fscanf(inpt, "%s", sffx);
    crtSvg(&sffx, pathOut, city, shapes, 1);
  }
  if (strcmp(comand, "pp") == 0) {
    fscanf(inpt, "%s %s %f", sffx, c1, &r);
    crtPrtl(&sffx, c1, r, pathOut, shapes);
  }
  if (strcmp(comand, "q") == 0) {
    Block aux;
    fscanf(inpt, "%f %f %f %f %s", &x, &y, &w, &h, cep);
    aux = crtBlck(x, y, w, h, cep, 'b');
    if (getBlcksTree(city) == NULL) {
      Tree blck = crtTree(&compTreeBlock, &dstrsBlck);
      setBlcksTree(city, blck);
    }
    addNode(getBlcksTree(city), aux);
    if (getBlocksHash(city) == NULL) {
      HashTable b1 = crtHashTable(1000);
      setBlocksHash(city, b1);
    }
    insereHash(getBlocksHash(city), aux, cep);
  }
  if (strcmp(comand, "h") == 0) {
    Equip aux;
    fscanf(inpt, "%s %f %f", cep, &x, &y);
    aux = crtEqp(x, y, cep, 'h');
    if (getHdrntTree(city) == NULL) {
      Tree hydra = crtTree(&compTreeEquip, &dstrsEquip);
      setHdrntTree(city, hydra);
    }
    addNode(getHdrntTree(city), aux);
    if (getEquipsHash(city) == NULL) {
      HashTable h1 = crtHashTable(4000);
      setEquipsHash(city, h1);
    }
    insereHash(getEquipsHash(city), aux, cep);
  }
  if (strcmp(comand, "s") == 0) {
    Equip aux;
    fscanf(inpt, "%s %f %f", cep, &x, &y);
    aux = crtEqp(x, y, cep, 't');
    if (getTrffcLghtTree(city) == NULL) {
      Tree traff = crtTree(&compTreeEquip, &dstrsEquip);
      setTrffcLghtTree(city, traff);
    }
    addNode(getTrffcLghtTree(city), aux);
    if (getEquipsHash(city) == NULL) {
      HashTable s1 = crtHashTable(4000);
      setEquipsHash(city, s1);
    }
    insereHash(getEquipsHash(city), aux, cep);
  }
  if (strcmp(comand, "t") == 0) {
    Equip aux;
    fscanf(inpt, "%s %f %f", cep, &x, &y);
    aux = crtEqp(x, y, cep, 'd');
    if (getRdBsTree(city) == NULL) {
      Tree radio = crtTree(&compTreeEquip, &dstrsEquip);
      setRdBsTree(city, radio);
    }
    addNode(getRdBsTree(city), aux);
    if (getEquipsHash(city) == NULL) {
      HashTable t1 = crtHashTable(4000);
      setEquipsHash(city, t1);
    }
    insereHash(getEquipsHash(city), aux, cep);
  }
  if (strcmp(comand, "ep?") == 0) {
    List lPlgn;
    FILE *inptPlg, *outTxt;
    fscanf(inpt, "%s %s", cep, pol);
    inptPlg = OpenFileTxt(pathInpt, &pol);
    lPlgn = creatPol(inptPlg);
    fclose(inptPlg);

    outTxt = CrtFileTxt(pathOut);
    if (outTxt == NULL)
      printf("Problemas com o arquivo de saida %s comando ep?\n", pathOut);
    else {
      fprintf(outTxt, "ep? %s %s\n", cep, pol);
      chckEqp(cep, getEquipsHash(city), lPlgn, outTxt);
      fclose(outTxt);
    }
  }
  if (strcmp(comand, "qp?") == 0) {
    List lPlgn;
    FILE *inptPlg, *outTxt;

    fscanf(inpt, "%s %s", cep, pol);

    inptPlg = OpenFileTxt(pathInpt, &pol);
    lPlgn = creatPol(inptPlg);
    fclose(inptPlg);

    outTxt = CrtFileTxt(pathOut);
    if (outTxt == NULL)
      printf("Problemas com o arquivo de saida %s comando qp?\n", pathOut);
    else {
      fprintf(outTxt, "qp? %s %s\n", cep, pol);
      chckBlck(cep, getBlocksHash(city), lPlgn, outTxt);
      fclose(outTxt);
    }
  }
  if (strcmp(comand, "qF?") == 0) {
    List lPlgn;
    FILE *inptPlg, *outTxt;

    fscanf(inpt, "%s %s", cep, pol);
    inptPlg = OpenFileTxt(pathInpt, &pol);
    lPlgn = creatPol(inptPlg);
    fclose(inptPlg);

    outTxt = CrtFileTxt(pathOut);
    if (outTxt == NULL)
      printf("Problemas com o arquivo de saida %s comando qF?\n", pathOut);
    else {
      fprintf(outTxt, "qF? %s %s\n", cep, pol);
      chckBlckFace(cep, getBlocksHash(city), lPlgn, outTxt);
      fclose(outTxt);
    }
  }
  if (strcmp(comand, "qf?") == 0) {
    List lPlgn;
    FILE *inptPlg, *outTxt;

    fscanf(inpt, "%s %s", cep, pol);
    inptPlg = OpenFileTxt(pathInpt, &pol);
    lPlgn = creatPol(inptPlg);
    fclose(inptPlg);

    outTxt = CrtFileTxt(pathOut);
    if (outTxt == NULL)
      printf("Problemas com o arquivo de saida %s comando qf?\n", pathOut);
    else {
      fprintf(outTxt, "qf? %s %s\n", cep, pol);
      chckBlckHlf(cep, getBlocksHash(city), lPlgn, outTxt);
      fclose(outTxt);
    }
  }
  if (strcmp(comand, "nt?") == 0) {
    FILE *outTxt;
    fscanf(inpt, "%d %c %f %f", &k, &o, &x, &y);
    outTxt = CrtFileTxt(pathOut);
    if (outTxt == NULL)
      printf("Problemas com o arquivo de saida %s comando nt?\n", pathOut);
    else {
      fprintf(outTxt, "%s %d %c %.2f %.2f\n", comand, k, o, x, y);
      if (getQtd(getRdBsTree(city)) < k)
        k = getQtd(getRdBsTree(city));
      Item closer = RdioBsClsr(city, shapes, x, y, k, o);
      crtCrclPntsClsr(shapes, closer, city, k - 1, x, y, o, n, outTxt);
      fclose(outTxt);
    }
  }
  if (strcmp(comand, "nh?") == 0) {
    FILE *outTxt;
    fscanf(inpt, "%d %c %f %f", &k, &o, &x, &y);
    outTxt = CrtFileTxt(pathOut);
    if (outTxt == NULL)
      printf("Problemas com o arquivo de saida %s comando nh?\n", pathOut);
    else {
      fprintf(outTxt, "%s %d %c %.2f %.2f\n", comand, k, o, x, y);
      HydrntClsr(city, shapes, x, y, k, o, outTxt, n);
      fclose(outTxt);
    }
  }
  if (strcmp(comand, "ft?") == 0) {
    FILE *outTxt;
    fscanf(inpt, "%d %c %f %f", &k, &o, &x, &y);
    outTxt = CrtFileTxt(pathOut);
    if (outTxt == NULL)
      printf("Problemas com o arquivo de saida %s comando ft?\n", pathOut);
    else {
      fprintf(outTxt, "%s %d %c %.2f %.2f\n", comand, k, o, x, y);
      RdioBsDist(city, shapes, x, y, k, o, outTxt, n);
      fclose(outTxt);
    }
  }
  if (strcmp(comand, "fh?") == 0) {
    FILE *outTxt;
    fscanf(inpt, "%d %c %f %f", &k, &o, &x, &y);
    outTxt = CrtFileTxt(pathOut);
    if (outTxt == NULL)
      printf("Problemas com o arquivo de saida %s comando fh?\n", pathOut);
    else {
      fprintf(outTxt, "%s %d %c %.2f %.2f\n", comand, k, o, x, y);
      HydrntDist(city, shapes, x, y, k, o, outTxt, n);
      fclose(outTxt);
    }
  }
  if (strcmp(comand, "nrt?") == 0) {
    FILE *outTxt;
    fscanf(inpt, "%d %d %c %f %f", &k1, &k2, &o, &x, &y);
    outTxt = CrtFileTxt(pathOut);
    if (outTxt == NULL)
      printf("Problemas com o arquivo de saida %s comando nrt?\n", pathOut);
    else {
      fprintf(outTxt, "%s %d %d %c %.2f %.2f\n", comand, k1, k2, o, x, y);
      RdioBsIntrvl(city, shapes, x, y, k1, k2, o, outTxt, n);
      fclose(outTxt);
    }
  }
  if (strcmp(comand, "nrh?") == 0) {
    FILE *outTxt;
    fscanf(inpt, "%d %d %c %f %f", &k1, &k2, &o, &x, &y);
    outTxt = CrtFileTxt(pathOut);
    if (outTxt == NULL)
      printf("Problemas com o arquivo de saida %s comando nrh?\n", pathOut);
    else {
      fprintf(outTxt, "%s %d %d %c %.2f %.2f\n", comand, k1, k2, o, x, y);
      HydrntIntrvl(city, shapes, x, y, k1, k2, o, outTxt, n);
      fclose(outTxt);
    }
  }
  // mq?
  if (strcmp(comand, "mq?") == 0) {
    fscanf(inpt, "%s", cep);
    FILE *outTxt = CrtFileTxt(pathOut);
    if (outTxt == NULL)
      printf("Problemas com o arquivo de saida %s comando mq?\n", pathOut);
    else {
      fprintf(outTxt, "%s %s\n", comand, cod);
      prcrMrds(outTxt, cep, city);
      fprintf(outTxt, "\n");
      fclose(outTxt);
    }
  }
  // mrr?
  if (strcmp(comand, "mrr?") == 0) {
    fscanf(inpt, "%f %f %f %f", &x1, &y1, &x2, &y2);
    FILE *outTxt = CrtFileTxt(pathOut);
    if (outTxt == NULL)
      printf("Problemas com o arquivo de saida %s comando mrr?\n", pathOut);
    else {
      int o = 0;
      fprintf(outTxt, "%s %f %f %f\n", comand, x1, y1, x2, y2);
      vrfcBlcksR(getHead(getBlcksTree(city)), x1, y1, x2, y2, outTxt, city, &o,
                 &getCepMoradoresHash, &escreveDadosMorador);
      if (o == 0)
        fprintf(outTxt, "Não existe quadra dentro dessa região\n");
      fprintf(outTxt, "\n");
      fclose(outTxt);
    }
  }
  // mrc?
  if (strcmp(comand, "mrc?") == 0) {
    fscanf(inpt, "%f %f %f", &x, &y, &r);
    FILE *outTxt = CrtFileTxt(pathOut);
    if (outTxt == NULL)
      printf("Problemas com o arquivo de saida %s comando mrc?\n", pathOut);
    else {
      int o = 0;
      fprintf(outTxt, "%s %f %f %f\n", comand, x, y, r);
      vrfcBlcksC(getHead(getBlcksTree(city)), x, y, r, outTxt, city, &o,
                 &getCepMoradoresHash, &escreveDadosMorador);
      if (o == 0)
        fprintf(outTxt, "Não existe quadra dentro dessa região\n");
      fprintf(outTxt, "\n");
      fclose(outTxt);
    }
  }
  if (strcmp(comand, "qet?") == 0) {
    fscanf(inpt, "%s", cod);
    FILE *outTxt = CrtFileTxt(pathOut);
    if (outTxt == NULL)
      printf("Problemas com o arquivo de saida %s comando qet?\n", pathOut);
    else {
      fprintf(outTxt, "%s %s\n", comand, cod);
      vrfcBlcksCodTp(outTxt, cod, city);
      fprintf(outTxt, "\n");
      fclose(outTxt);
    }
  }
  if (strcmp(comand, "qetr?") == 0) {
    fscanf(inpt, "%s %f %f %f %f", cod, &x1, &y1, &x2, &y2);
    FILE *outTxt = CrtFileTxt(pathOut);
    if (outTxt == NULL)
      printf("Problemas com o arquivo de saida %s comando qetr?\n", pathOut);
    else {
      int o = 0;
      fprintf(outTxt, "%s %s %f %f %f %f\n", comand, cod, x1, y1, x2, y2);
      if (strcmp(cod, "*") != 0)
        vrfcBlcksQetCodTp(x1, y1, x2, y2, outTxt, city, cod);
      else
        vrfcBlcksR(getHead(getBlcksTree(city)), x1, y1, x2, y2, outTxt, city,
                   &o, &getCepComercialHash, &escreveNomeEndDes);
      if (o == 0 && strcmp(cod, "*") == 0)
        fprintf(outTxt, "Não existe quadra dentro dessa região\n");
      fprintf(outTxt, "\n");
      fclose(outTxt);
    }
  }
  if (strcmp(comand, "etp?") == 0) {
    fscanf(inpt, "%s %d %f %f", cod, &k, &x, &y);
    FILE *outTxt = CrtFileTxt(pathOut);
    if (outTxt == NULL)
      printf("Problemas com o arquivo de saida %s comando etp?\n", pathOut);
    else {
      fprintf(outTxt, "%s %s %d %f %f\n", comand, cod, k, x, y);
      kEstCom(cod, k, x, y, city, outTxt);
      fprintf(outTxt, "\n");
      fclose(outTxt);
    }
  }
}
