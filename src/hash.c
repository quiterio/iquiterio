#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"
#include "hash.h"

typedef struct hsh{
  Item thing;
  struct hsh *prox;
  char* palavra;
}hash;

typedef struct hdhsh{
  HashTable *lista;
  int len;
}hashHead;

HashTable crtHsh(int tamanho){
  HashTable tabelaHash = malloc(tamanho*sizeof(HashTable));
  return tabelaHash;
}

void removeCell(hash *anterior, hash *atual, hashHead *aux, int index);

HashTable crtHashTable(int tamanho){
  hashHead *head = malloc (sizeof (hashHead));
  head->len = tamanho;
  head->lista = crtHsh(tamanho);
  HashTable *aux = head->lista;
  for(int i=0;i<tamanho;i++){
    aux[i] = NULL;
  }
  return head;
}

hash *crtCell(Item anything, char* key){
  hash *aux = malloc(sizeof(hash));
  aux->thing = anything;
  aux->palavra = strdup(key);
  aux->prox = NULL;
  return aux;
}

int crtCod(char* key,int sizemattersbro){ // jk sz doesn mttr
  int soma = 0, i;
  int tam = strlen(key);
  int vetor[15] = {1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987};
  for (i = 0; i < tam; i++) {
    soma+= (int)key[i]*vetor[i%15];
  }
  return soma%sizemattersbro;
}


void insereHash (HashTable hashTb, Item anything, char* key){
  hashHead *head = (hashHead*)hashTb;
  int index = crtCod(key,getLenHash(head));
  HashTable *vetor = head->lista;
  hash *new;
  hash *aux;
  new = crtCell(anything,key);
  if (vetor[index] == NULL){
    vetor[index] = new;
  }else{
    aux = vetor[index];
    while(aux->prox != NULL){
      aux = aux->prox;
    }
    aux->prox = new;
  }
}

void fndKeyList(HashTable head, char *key, List *lista){
  hashHead *aux = (hashHead*)head;
  HashTable *vetor = aux->lista;
  int index = crtCod(key,getLenHash(aux));
  if(vetor[index] != NULL){
    hash *aux1 = vetor[index];
    while(aux1 != NULL){
      if(strcmp(key,aux1->palavra) == 0) pushFront(lista,aux1->thing);
      aux1 = aux1->prox;
    }
  }
}

Item fndKey(HashTable head, char* key){
  hashHead *aux = (hashHead*)head;
  if(aux != NULL){
    HashTable *vetor = aux->lista;
    int index = crtCod(key,getLenHash(aux));
    if(vetor[index] != NULL){
      hash *aux1 = vetor[index];
      while(aux1 != NULL){
        if(strcmp(aux1->palavra,key) == 0) return aux1->thing;
        aux1 = aux1->prox;
      }
    }
  }
  return NULL;
}

int getLenHash(HashTable pHash){
  hashHead *aux = (hashHead*)pHash;
  if(aux != NULL) return aux->len;
  return 0;
}

void dltKeyHash(HashTable head, char *key){
  hashHead *aux = (hashHead*)head;
  if(aux != NULL){
    HashTable *vetor = aux->lista;
    int index = crtCod(key,getLenHash(aux));
    if(vetor[index] != NULL){
      hash *atual = vetor[index];
      hash *anterior = NULL;
      while(atual != NULL){
        if(strcmp(atual->palavra,key) == 0) removeCell(anterior,atual,aux,index);
        anterior = atual;
        atual = atual->prox;
      }
    }
  }
}

void removeCell(hash *anterior, hash *atual, hashHead *aux, int index){
  if(anterior == NULL){
    HashTable *vetor = aux->lista;
    vetor[index] = atual->prox;
  }
  else anterior->prox = atual->prox;
  free(atual);
}

void dltKeyListHash(HashTable head, char *key, Item data, int (*funComp) (Item,Item)){
  hashHead *aux = (hashHead*)head;
  if(aux != NULL){
    HashTable *vetor = aux->lista;
    int index = crtCod(key,getLenHash(aux));
    if(vetor[index] != NULL){
      hash *anterior = NULL;
      hash *atual = vetor[index];
      while(atual != NULL){
        if(funComp(atual->thing,data) == 0) removeCell(anterior,atual,aux,index);
        anterior = atual;
        atual = atual->prox;
      }
    }
  }
}
