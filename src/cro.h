#ifndef CRO_H_
#define CRO_H_

#include <stdio.h>
#include "list.h"
#include "city.h"
/*Arquivo .cro é um arquivo de texto que contém em sua primeira linha
um numero inteiro representando o numero maximo de comandos ’r’, ’c’, ’q’, ’h’, ’t’, e ’s’ definidos a seguir.
Seguido na proxima linha por dois números reais maiores que zero,que serão respectivamente a largura ea altura do arquivo svg(SVG).
Após isso segue as linhas de comando possíveis, uma linha de começadapor ’c’ seguida de 3 numeros reais maiores que zero e 1 numero inteiro,
ou começada por ’r’ seguida de4 numeros reais e 1 numero inteiro, ou começada por ’o’ seguida de 2 numeros inteiros, ou começadapor ’cr’
seguida de 2 strings, strings correspondentes as cores padrão SVG, ou começada por ’cc’ seguidade 2 strings, strings correspondentes as cores
padrão SVG, ou começada por ’xc’ seguida por 3 numerosreais maiores que zero e 2 strings, strings correspondentes as cores padrão SVG,
ou começada por ’xr’seguida por 4 numeros reais maiores que zero e 2 strings, strisgs correspondentes as cores padrão SVG,ou começada por ’ps’
seguida por 1 string, ou começada por ’pp’ seguida por 2 strings, sendo a segundastring correspondente a uma cor aceita pelo padrão SVG,
e 1 numero real maior que zero, ou começadapor ’q’ seguida por 4 numeros reais maiores que zero e 1 string, ou começada por ’h’, ’s’ ou ’t’
seguida por1 string e 2 números reais maiores que zero, ou ainda começada por ’ep?’, ’qp?’, ’qF?’ ou ’qf?’ seguida por2 string, sendo a segunda
relativa a um arquivo texto para a leitura dos vértices de um polígono(Polygon).Após as linhas de comando, para o término do arquivo .cro deve
ter o caractere ’’ que finaliza a leitura.*/
void slctCmd(FILE* inpt, char **pathOut, char **pathInpt, char* comand, char *FllC, char *StrkC, char *FllR, char *StrkR, City city, List *shapes, int n);
/*npt arquivo .cro aberto e posicionado a uma linha de comando aceita.  Listblocks, lShp e equips relacionados respectivamente as
listas de quadra(Block), Shapes e equipa-mentos(Equip).  Width e height maior que zero respectivos a largura e altura lido no inicio do
arquivo .cro. PathOut e pathInpt relativos ao diretório de saida e de entrada respectivamente. FllC,StrkC, FllR e StrkR strings relativas
as cores aceitas pelo padrao SVG. GeoNmb relativo a primeiralinha do arquivo .cro já lida. Comand uma string relativa aos possiveis comandos,
’q’, ’r’, ’c’, ’o’,’ep?’, etc*/
//Verifica o comando selecionado e executa as instruções correspondentes
void arqCro(FILE *inpt, char **pathOut, char **pathInpt, City city, List *shapes);

#endif
